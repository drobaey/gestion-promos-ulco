package fr.univ_littoral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fr.univ_littoral")
public class Api {

    public static void main(String[] args) {
        SpringApplication.run(Api.class, args);
    }

}