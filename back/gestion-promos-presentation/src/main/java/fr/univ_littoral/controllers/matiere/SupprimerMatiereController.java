package fr.univ_littoral.controllers.matiere;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import fr.univ_littoral.service.matiere.IMatiereService;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class SupprimerMatiereController {
    private IMatiereService matiereService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerMatiereController.class);

    private static final String ERREUR_CLASSE_SUPPRIMER_MATIERES = "Erreur dans la classe SupprimerMatiereController";

    /**
     * Controller permettant de supprimer une matière
     *
     * @param id  id de la matière
     * @return
     */
    @DeleteMapping("/matiere/{id}")
    public boolean supprimerMatiere(@PathVariable final int id) {
        try {
            matiereService.deleteMatiereById(id);
            return true;
        } catch (Exception e) {

            // Si la matière est utilisée dans une autre table, on renvoie une erreur appropriée
            if (e instanceof DataIntegrityViolationException) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "La matière est utilisée dans une autre table");
            }

            LOGGER.error(ERREUR_CLASSE_SUPPRIMER_MATIERES, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setMatiereService(IMatiereService matiereService) {
        this.matiereService = matiereService;
    }
}