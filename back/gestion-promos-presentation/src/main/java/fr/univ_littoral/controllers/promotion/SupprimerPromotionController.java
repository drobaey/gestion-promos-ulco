package fr.univ_littoral.controllers.promotion;

import fr.univ_littoral.service.promotion.IPromoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class SupprimerPromotionController {
    private IPromoService promo_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(SupprimerPromotionController.class);

    private static final String ERREUR_CLASSE_SUPPRIMER_PROMO = "Erreur dans la classe SupprimerPromotionController";

    /**
     * Controller permettant de supprimer une promotion
     *
     * @param id  id de la promotion
     * @return
     */
    @DeleteMapping("/promotion/{id}")
    public boolean supprimerPromo(@PathVariable final int id) {
        try {
            promo_service.deletePromoById(id);
            return true;
        } catch (Exception e) {
            // Si la promotion est utilisée dans une autre table, on renvoie une erreur appropriée
            if (e instanceof DataIntegrityViolationException) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "La promotion est utilisée dans une autre table");
            }

            LOGGER.error(ERREUR_CLASSE_SUPPRIMER_PROMO, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setPromoService(IPromoService promo_service) {
        this.promo_service = promo_service;
    }
}
