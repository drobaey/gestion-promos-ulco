package fr.univ_littoral.controllers.promotion;

import fr.univ_littoral.controllers.promotion.CreerPromotionController;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.promotion.IPromoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class DetailsPromotionController {
    private IPromoService promo_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(DetailsPromotionController.class);

    private static final String ERREUR_CLASSE_DETAIL_NOTE = "Erreur dans la classe DetailsPromotionController";

    /**
     * Controller permettant de récupérer les détails d'une promotion
     *
     * @param id  id de la promotion
     * @return
     */
    @GetMapping("/promotion/{id}")
    public PromoDTO getDetailsPromotion(@PathVariable final int id) {
        try {
            PromoDTO promo_DTO = promo_service.findPromoById(id);

            if (promo_DTO == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "promo non trouvée");
            }

            return promo_DTO;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_DETAIL_NOTE, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setPromoService(IPromoService promoService) {
        this.promo_service = promoService;
    }
}