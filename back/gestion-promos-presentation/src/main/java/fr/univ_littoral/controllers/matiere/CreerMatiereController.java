package fr.univ_littoral.controllers.matiere;

import fr.univ_littoral.dto.MatiereDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import fr.univ_littoral.service.matiere.IMatiereService;

@CrossOrigin
@RestController
public class CreerMatiereController {

    private IMatiereService matiereService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerMatiereController.class);

    private static final String ERREUR_CLASSE_CREER_MATIERES = "Erreur dans la classe CreerMatiereController";

    /**
     * Controller permettant de créer une matière
     *
     * @param matiereDTO  matière à créer
     * @return
     */
    @PostMapping("/matiere")
    public MatiereDTO creerMatiere(@RequestBody MatiereDTO matiereDTO) {
        try {
           return matiereService.createMatiere(matiereDTO);
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_CREER_MATIERES, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setMatiereService(IMatiereService matiereService) {
        this.matiereService = matiereService;
    }
}
