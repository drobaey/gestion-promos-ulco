package fr.univ_littoral.controllers.matiere;

import fr.univ_littoral.dto.MatiereDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import fr.univ_littoral.service.matiere.IMatiereService;

import java.util.List;

@CrossOrigin
@RestController
public class ListerMatiereController {
    private IMatiereService matiereService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerMatiereController.class);

    private static final String ERREUR_CLASSE_LISTER_MATIERES = "Erreur dans la classe ListerMatiereController";

    /**
     * Controller permettant de récupérer toutes les matières
     *
     * @return
     */
    @GetMapping("/matiere")
    public List<MatiereDTO> listerMatieres() {
        try {
            return matiereService.listMatiere();
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_LISTER_MATIERES, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setMatiereService(IMatiereService matiereService) {
        this.matiereService = matiereService;
    }
}