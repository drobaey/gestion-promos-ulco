package fr.univ_littoral.controllers.promotion;

import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.promotion.IPromoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ModifierPromotionController {
    private IPromoService promo_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifierPromotionController.class);

    private static final String ERREUR_CLASSE_MODIFIER_PROMO = "Erreur dans la classe ModifierPromotionController";

    /**
     * Controller permettant de modifier une promotion
     *
     * @param id  id de la promotion
     * @param promo_DTO  note mise à jour
     * @return
     */
    @PutMapping("/promotion/{id}")
    public boolean modifierPromo(@PathVariable final int id, @RequestBody final PromoDTO promo_DTO) {
        try {
            promo_service.updatePromo(promo_DTO);
            return true;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_MODIFIER_PROMO, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setPromoService(IPromoService noteService) {
        this.promo_service = noteService;
    }
}