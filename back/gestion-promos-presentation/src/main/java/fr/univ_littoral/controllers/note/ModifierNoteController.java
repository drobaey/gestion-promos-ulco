package fr.univ_littoral.controllers.note;

import fr.univ_littoral.controllers.note.CreerNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ModifierNoteController {
    private INoteService note_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifierNoteController.class);

    private static final String ERREUR_CLASSE_MODIFIER_NOTE = "Erreur dans la classe ModifierNoteController";

    /**
     * Controller permettant de modifier une note
     *
     * @param id  id de la note
     * @param note_DTO  note mise à jour
     * @return
     */
    @PutMapping("/note/{id}")
    public boolean modifierNote(@PathVariable final int id, @RequestBody final NoteDTO note_DTO) {
        try {
            note_service.updateNote(note_DTO);
            return true;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_MODIFIER_NOTE, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setNoteService(INoteService noteService) {
        this.note_service = noteService;
    }
}