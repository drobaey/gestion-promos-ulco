package fr.univ_littoral.controllers.etudiant;

import fr.univ_littoral.controllers.matiere.CreerMatiereController;
import fr.univ_littoral.service.etudiant.IEtudiantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class SupprimerEtudiantController {
    private IEtudiantService etudiant_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(SupprimerEtudiantController.class);

    private static final String ERREUR_CLASSE_SUPPRIMER_ETUDIANT = "Erreur dans la classe SupprimerEtudiantController";

    /**
     * Controller permettant de supprimer un étudiant
     *
     * @param id  id de l'étudiant
     * @return
     */
    @DeleteMapping("/etudiant/{id}")
    public boolean supprimerEtudiant(@PathVariable final int id) {
        try {
            etudiant_service.deleteEtudiantById(id);
            return true;
        } catch (Exception e) {

            // Si l'étudiant est référencé dans une autre table, on renvoie une erreur appropriée
            if (e instanceof DataIntegrityViolationException) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "L'étudiant est référencé dans une autre table");
            }

            LOGGER.error(ERREUR_CLASSE_SUPPRIMER_ETUDIANT, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setEtudiant_service(IEtudiantService etudiant_service) {
        this.etudiant_service = etudiant_service;
    }
}
