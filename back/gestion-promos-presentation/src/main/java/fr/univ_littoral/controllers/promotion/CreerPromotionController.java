package fr.univ_littoral.controllers.promotion;

import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.promotion.IPromoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CreerPromotionController {
    private IPromoService promo_service ;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreerPromotionController.class);
    private static final String ERREUR_CLASSE_CREER_PROMO = "Erreur dans la classe CreerPromoController";

    /**
     * Controller permettant de créer une promotion
     *
     * @param promo_DTO promotion à créer
     * @return
     */
    @PostMapping("/promotion")
    public PromoDTO creerPromotion(@RequestBody PromoDTO promo_DTO) {
        try {
            return promo_service.createPromo(promo_DTO);
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_CREER_PROMO, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setPromoService(IPromoService promo_service) {
        this.promo_service = promo_service;
    }

}
