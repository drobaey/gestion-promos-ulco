package fr.univ_littoral.controllers.etudiant;

import fr.univ_littoral.controllers.matiere.CreerMatiereController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.IEtudiantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class ModifierEtudiantController {
    private IEtudiantService etudiant_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ModifierEtudiantController.class);

    private static final String ERREUR_CLASSE_MODIFIER_ETUDIANT = "Erreur dans la classe ModifierEtudiantController";

    /**
     * Controller permettant de modifier un étudiant
     *
     * @param id  id de l'étudiant
     * @param etudiant_DTO  étudiant mis à jour
     * @return
     */
    @PutMapping("/etudiant/{id}")
    public boolean modifierEtudiant(@PathVariable final int id, @RequestBody final EtudiantDTO etudiant_DTO) {
        try {
            etudiant_service.updateEtudiant(etudiant_DTO);
            return true;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_MODIFIER_ETUDIANT, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setEtudiantService(IEtudiantService etudiant_service) {
        this.etudiant_service = etudiant_service;
    }
}
