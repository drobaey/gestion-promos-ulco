package fr.univ_littoral.controllers.promotion;

import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.promotion.IPromoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class ListerPromotionController {
    private IPromoService promo_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ListerPromotionController.class);

    private static final String ERREUR_CLASSE_LISTER_PROMOTION = "Erreur dans la classe ListerPromotionController";

    /**
     * Controller permettant de récupérer toutes les promotions
     *
     * @return
     */
    @GetMapping("/promotion")
    public List<PromoDTO> listerPromotion() {
        try {
            return promo_service.listPromos();
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_LISTER_PROMOTION, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setPromoService(IPromoService promoService) {
        this.promo_service = promoService;
    }
}