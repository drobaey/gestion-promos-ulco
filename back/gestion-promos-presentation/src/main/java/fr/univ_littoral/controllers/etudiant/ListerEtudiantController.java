package fr.univ_littoral.controllers.etudiant;

import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.IEtudiantService;
import fr.univ_littoral.service.matiere.IMatiereService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class ListerEtudiantController {
    private IEtudiantService etudiant_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ListerEtudiantController.class);

    private static final String ERREUR_CLASSE_LISTER_ETUDIANT = "Erreur dans la classe ListerEtudiantController";

    /**
     * Controller permettant de récupérer toutes les étudiants
     *
     * @return
     */
    @GetMapping("/etudiant")
    public List<EtudiantDTO> listerEtudiant() {
        try {
            return etudiant_service.listEtudiants();
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_LISTER_ETUDIANT, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setEtudiant_service(IEtudiantService etudiant_service) {
        this.etudiant_service = etudiant_service;
    }
}
