package fr.univ_littoral.controllers.etudiant;

import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.IEtudiantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class DetailEtudiantController {
    private IEtudiantService etudiant_service ;
    private static final Logger LOGGER = LoggerFactory.getLogger(DetailEtudiantController.class);
    private static final String ERREUR_CLASSE_DETAIL_ETUDIANT = "Erreur dans la classe DetailEtudiantController";

    /**
     * Controller permettant de récupérer les détails d'un étudiant
     *
     * @param id  id de l'étudiant
     * @return
     */
    @GetMapping("/etudiant/{id}")
    public EtudiantDTO getDetailsEtudiant(@PathVariable final int id) {
        try {
            EtudiantDTO etudiant_DTO = etudiant_service.findEtudiantById(id);

            if (etudiant_DTO == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Étudiant non trouvée");
            }

            return etudiant_DTO;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_DETAIL_ETUDIANT, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setEtudiantService(IEtudiantService etudiant_service) {
        this.etudiant_service = etudiant_service;
    }
}
