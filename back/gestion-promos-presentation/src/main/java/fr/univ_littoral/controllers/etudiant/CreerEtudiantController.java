package fr.univ_littoral.controllers.etudiant;

import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.IEtudiantService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CreerEtudiantController {
    private IEtudiantService etudiant_service ;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreerEtudiantController.class);
    private static final String ERREUR_CLASSE_CREER_ETUDIANT = "Erreur dans la classe CreerEtudiantController";

    /**
     * Controller permettant de créer un étudiant
     *
     * @param etudiant_DTO étudiant à créer
     * @return
     */
    @PostMapping("/etudiant")
    public EtudiantDTO creerEtudiant(@RequestBody EtudiantDTO etudiant_DTO) {
        try {
            return etudiant_service.createEtudiant(etudiant_DTO);
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_CREER_ETUDIANT, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setEtudiantService(IEtudiantService etudiant_service) {
        this.etudiant_service = etudiant_service;
    }

}
