package fr.univ_littoral.controllers.note;

import fr.univ_littoral.controllers.note.CreerNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class ListerNoteController {
    private INoteService note_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(ListerNoteController.class);

    private static final String ERREUR_CLASSE_LISTER_NOTE = "Erreur dans la classe ListerNoteController";

    /**
     * Controller permettant de récupérer toutes les notes
     *
     * @return
     */
    @GetMapping("/note")
    public List<NoteDTO> listerNote() {
        try {
            return note_service.listNotes();
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_LISTER_NOTE, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setNoteService(INoteService noteService) {
        this.note_service = noteService;
    }
}