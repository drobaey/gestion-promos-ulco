package fr.univ_littoral.controllers.matiere;

import fr.univ_littoral.dto.MatiereDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import fr.univ_littoral.service.matiere.IMatiereService;

@CrossOrigin
@RestController
public class ModifierMatiereController {
    private IMatiereService matiereService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerMatiereController.class);

    private static final String ERREUR_CLASSE_MODIFIER_MATIERES = "Erreur dans la classe ModifierMatiereController";

    /**
     * Controller permettant de modifier une matière
     *
     * @param id  id de la matière
     * @param matiereDTO  matière mise à jour
     * @return
     */
    @PutMapping("/matiere/{id}")
    public boolean modifierMatiere(@PathVariable final int id, @RequestBody final MatiereDTO matiereDTO) {
        try {
            matiereService.updateMatiere(matiereDTO);
            return true;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_MODIFIER_MATIERES, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setMatiereService(IMatiereService matiereService) {
        this.matiereService = matiereService;
    }
}