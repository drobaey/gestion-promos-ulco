package fr.univ_littoral.controllers.matiere;

import fr.univ_littoral.dto.MatiereDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import fr.univ_littoral.service.matiere.IMatiereService;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class DetailsMatiereController {
    private IMatiereService matiereService;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerMatiereController.class);

    private static final String ERREUR_CLASSE_DETAIL_MATIERES = "Erreur dans la classe DetailsMatiereController";

    /**
     * Controller permettant de récupérer les détails d'une matière
     *
     * @param id  id de la matière
     * @return
     */
    @GetMapping("/matiere/{id}")
    public MatiereDTO getDetailsMatiere(@PathVariable final int id) {
        try {
            MatiereDTO matiereDTO = matiereService.findMatiereById(id);

            if (matiereDTO == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Matière non trouvée");
            }

            return matiereDTO;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_DETAIL_MATIERES, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setMatiereService(IMatiereService matiereService) {
        this.matiereService = matiereService;
    }
}