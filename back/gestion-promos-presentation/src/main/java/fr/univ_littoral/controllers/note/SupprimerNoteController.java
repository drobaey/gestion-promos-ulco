package fr.univ_littoral.controllers.note;

import fr.univ_littoral.service.note.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class SupprimerNoteController {
    private INoteService note_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(SupprimerNoteController.class);

    private static final String ERREUR_CLASSE_SUPPRIMER_NOTE = "Erreur dans la classe SupprimerNoteController";

    /**
     * Controller permettant de supprimer une note
     *
     * @param id  id de la note
     * @return
     */
    @DeleteMapping("/note/{id}")
    public boolean supprimerNote(@PathVariable final int id) {
        try {
            note_service.deleteNoteById(id);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setNoteService(INoteService note_service) {
        this.note_service = note_service;
    }
}
