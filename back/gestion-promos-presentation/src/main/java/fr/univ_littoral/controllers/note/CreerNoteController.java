package fr.univ_littoral.controllers.note;

import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CreerNoteController {
    private INoteService note_service ;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreerNoteController.class);
    private static final String ERREUR_CLASSE_CREER_NOTE = "Erreur dans la classe CreerNoteController";

    /**
     * Controller permettant de créer une Note
     *
     * @param note_DTO note à créer
     * @return
     */
    @PostMapping("/note")
    public NoteDTO creerNote(@RequestBody NoteDTO note_DTO) {
        try {
            return note_service.createNote(note_DTO);
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_CREER_NOTE, e);
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void setNoteService(INoteService note_service) {
        this.note_service = note_service;
    }

}
