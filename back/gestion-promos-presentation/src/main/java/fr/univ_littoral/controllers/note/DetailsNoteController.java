package fr.univ_littoral.controllers.note;

import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.INoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
public class DetailsNoteController {
    private INoteService note_service;

    private static final Logger LOGGER = LoggerFactory.getLogger(CreerNoteController.class);

    private static final String ERREUR_CLASSE_DETAIL_NOTE = "Erreur dans la classe DetailsNoteController";

    /**
     * Controller permettant de récupérer les détails d'une note
     *
     * @param id  id de la note
     * @return
     */
    @GetMapping("/note/{id}")
    public NoteDTO getDetailsNote(@PathVariable final int id) {
        try {
            NoteDTO note_DTO = note_service.findNoteById(id);

            if (note_DTO == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Note non trouvée");
            }

            return note_DTO;
        } catch (Exception e) {
            LOGGER.error(ERREUR_CLASSE_DETAIL_NOTE, e);
            throw new RuntimeException(e);
        }
    }


    @Autowired
    public void setNoteService(INoteService noteService) {
        this.note_service = noteService;
    }
}