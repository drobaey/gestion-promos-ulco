package fr.univ_littoral.controller.note;

import fr.univ_littoral.controllers.matiere.DetailsMatiereController;
import fr.univ_littoral.controllers.note.DetailsNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import fr.univ_littoral.service.note.NoteServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DetailsNoteController.class)
public class ControllerDetailNoteTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private NoteServiceImpl service ;

    @Test
    public void DetailNote() throws Exception
    {
        NoteDTO noteDTO = new NoteDTO() ;
        noteDTO.setNote(17);
        noteDTO.setId(42);

        given(service.findNoteById(noteDTO.getId())).willReturn(noteDTO) ;

        mvc.perform(get("/note/"+noteDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
    }

    @Test
    public void should_throw_exception_when_note_doesnt_exist() throws Exception {
        NoteDTO matiereDTO = new NoteDTO();
        matiereDTO.setId(42);
        matiereDTO.setNote(17);
        Mockito.doThrow(new Exception()).when(service).findNoteById(matiereDTO.getId());
        mvc.perform(get("/note/" + matiereDTO.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
