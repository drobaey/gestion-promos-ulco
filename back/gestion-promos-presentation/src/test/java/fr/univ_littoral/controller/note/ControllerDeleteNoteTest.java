package fr.univ_littoral.controller.note;

import fr.univ_littoral.controllers.note.SupprimerNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SupprimerNoteController.class)
public class ControllerDeleteNoteTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private NoteServiceImpl service ;

    @Test
    public void removeNote() throws Exception
    {
        NoteDTO noteDTO = new NoteDTO() ;
        noteDTO.setNote(17);
        noteDTO.setId(42);

        doNothing().when(service).deleteNoteById(noteDTO.getId());

        mvc.perform(delete("/note/"+ noteDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent()) ;
    }

    @Test
    public void removeWhenNoteNotExist() throws Exception
    {
        NoteDTO noteDTO = new NoteDTO() ;
        noteDTO.setId(42);
        noteDTO.setNote(17);

        Mockito.doThrow(new Exception()).when(service).deleteNoteById(noteDTO.getId());

        mvc.perform(delete("/note/"+noteDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
