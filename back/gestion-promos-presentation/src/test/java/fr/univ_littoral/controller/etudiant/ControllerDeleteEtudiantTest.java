package fr.univ_littoral.controller.etudiant;

import fr.univ_littoral.controllers.etudiant.SupprimerEtudiantController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SupprimerEtudiantController.class)
public class ControllerDeleteEtudiantTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private EtudiantServiceImpl service ;

    @Test
    public void removeEtudiant() throws Exception
    {
        EtudiantDTO etudiantDTO = new EtudiantDTO() ;
        etudiantDTO.setNom("nom test");
        etudiantDTO.setId(42);

        doNothing().when(service).deleteEtudiantById(etudiantDTO.getId());

        mvc.perform(delete("/etudiant"+ etudiantDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent()) ;
    }

    @Test
    public void removeWhenEtudiantNotExist() throws Exception
    {
        EtudiantDTO etudiantDTO = new EtudiantDTO() ;
        etudiantDTO.setId(42);
        etudiantDTO.setNom("jon do");

        Mockito.doThrow(new Exception()).when(service).deleteEtudiantById(etudiantDTO.getId());

        mvc.perform(delete("etudiant/"+etudiantDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
