package fr.univ_littoral.controller.matiere;

import fr.univ_littoral.controllers.etudiant.SupprimerEtudiantController;
import fr.univ_littoral.controllers.matiere.SupprimerMatiereController;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SupprimerMatiereController.class)
public class ControllerDeleteMatiereTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private MatiereServiceImpl service ;

    @Test
    public void removeEtudiant() throws Exception
    {
        MatiereDTO matiereDTO = new MatiereDTO() ;
        matiereDTO.setIntitule("nom test");
        matiereDTO.setId(42);

        doNothing().when(service).deleteMatiereById(matiereDTO.getId());

        mvc.perform(delete("/matiere/"+ matiereDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent()) ;
    }

    @Test
    public void removeWhenMatiereNotExist() throws Exception
    {
        MatiereDTO matiereDTO = new MatiereDTO() ;
        matiereDTO.setId(42);
        matiereDTO.setIntitule("intitule test");

        Mockito.doThrow(new Exception()).when(service).deleteMatiereById(matiereDTO.getId());

        mvc.perform(delete("/matiere/"+matiereDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
