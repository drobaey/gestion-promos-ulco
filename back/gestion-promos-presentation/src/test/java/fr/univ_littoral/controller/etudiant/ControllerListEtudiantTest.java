package fr.univ_littoral.controller.etudiant;

import fr.univ_littoral.controllers.etudiant.ListerEtudiantController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ListerEtudiantController.class)
public class ControllerListEtudiantTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private EtudiantServiceImpl service ;

    @Test
    public void listAllEtudiant() throws Exception
    {
        EtudiantDTO etudiantDTO = new EtudiantDTO() ;
        etudiantDTO.setNom("nom test");
        List<EtudiantDTO> etudiants_list = Arrays.asList(etudiantDTO) ;

        given(service.listEtudiants()).willReturn(etudiants_list) ;

        mvc.perform(get("/etudiant")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].nom", is(etudiantDTO.getNom()))) ;
    }
}
