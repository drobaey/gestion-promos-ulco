package fr.univ_littoral.controller.promotion;

import fr.univ_littoral.controllers.note.ListerNoteController;
import fr.univ_littoral.controllers.promotion.ListerPromotionController;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ListerPromotionController.class)
public class ControllerListPromotionTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private PromoServiceImpl service ;

    @Test
    public void listAllEtudiant() throws Exception
    {
        PromoDTO promoDTO = new PromoDTO() ;
        promoDTO.setIntitule("intitule");
        List<PromoDTO> promotion_list = List.of(promoDTO);

        given(service.listPromos()).willReturn(promotion_list) ;

        mvc.perform(get("/promotion")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].intitule", is(promoDTO.getIntitule()))) ;
    }
}
