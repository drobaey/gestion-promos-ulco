package fr.univ_littoral.controller.promotion;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controller.JsonUtil;
import fr.univ_littoral.controllers.note.CreerNoteController;
import fr.univ_littoral.controllers.promotion.CreerPromotionController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreerPromotionController.class)
public class ControllerCreerPromotionTest {
    @Autowired
    private MockMvc mockMvc ;

    @MockBean
    private PromoServiceImpl service ;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createPromotion() throws Exception
    {
        PromoDTO promoDTO = new PromoDTO() ;
        promoDTO.setIntitule("intitule");

        given(service.createPromo(promoDTO)).willReturn(promoDTO) ;

        mockMvc.perform(post("/promotion")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(promoDTO)))
                .andExpect(status().isCreated()) ;
    }
}
