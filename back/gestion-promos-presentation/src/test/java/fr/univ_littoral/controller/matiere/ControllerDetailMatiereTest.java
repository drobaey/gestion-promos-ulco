package fr.univ_littoral.controller.matiere;

import fr.univ_littoral.controllers.etudiant.DetailEtudiantController;
import fr.univ_littoral.controllers.matiere.DetailsMatiereController;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DetailsMatiereController.class)
public class ControllerDetailMatiereTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private MatiereServiceImpl service ;

    @Test
    public void DetailMatiere() throws Exception
    {
        MatiereDTO matiereDTO = new MatiereDTO() ;
        matiereDTO.setIntitule("intitule test");
        matiereDTO.setId(42);

        given(service.findMatiereById(matiereDTO.getId())).willReturn(matiereDTO) ;

        mvc.perform(get("/matiere/"+matiereDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
    }

    @Test
    public void should_throw_exception_when_matiere_doesnt_exist() throws Exception {
        MatiereDTO matiereDTO = new MatiereDTO();
        matiereDTO.setId(42);
        matiereDTO.setIntitule("Test Name");
        Mockito.doThrow(new Exception()).when(service).findMatiereById(matiereDTO.getId());
        mvc.perform(get("/matiere/" + matiereDTO.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
