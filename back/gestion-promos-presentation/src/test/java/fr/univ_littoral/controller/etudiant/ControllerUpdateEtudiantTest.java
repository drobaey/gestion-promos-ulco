package fr.univ_littoral.controller.etudiant;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controllers.etudiant.ModifierEtudiantController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ModifierEtudiantController.class)
public class ControllerUpdateEtudiantTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private EtudiantServiceImpl service;
    @Test
    public void updateUser_whenPutUser() throws Exception {
        EtudiantDTO etudiantDTO = new EtudiantDTO();
        etudiantDTO.setNom("Test Name");
        etudiantDTO.setId(42);
        given(service.updateEtudiant(etudiantDTO)).willReturn(etudiantDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/etudiant"+ etudiantDTO.getId())
            .content(mapper.writeValueAsString(etudiantDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("nom", is(etudiantDTO.getNom())));
    }
    @Test
    public void should_throw_exception_when_etudiant_doesnt_exist() throws Exception {
        EtudiantDTO etudiantDTO = new EtudiantDTO();
        etudiantDTO.setId(42);
        etudiantDTO.setNom("Test name");
        Mockito.doThrow(new Exception()).when(service).updateEtudiant(etudiantDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/etudiant/" + etudiantDTO.getId())
            .content(mapper.writeValueAsString(etudiantDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
