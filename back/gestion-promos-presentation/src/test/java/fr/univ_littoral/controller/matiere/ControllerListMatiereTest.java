package fr.univ_littoral.controller.matiere;

import fr.univ_littoral.controllers.etudiant.ListerEtudiantController;
import fr.univ_littoral.controllers.matiere.ListerMatiereController;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ListerMatiereController.class)
public class ControllerListMatiereTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private MatiereServiceImpl service ;

    @Test
    public void listAllEtudiant() throws Exception
    {
        MatiereDTO matiereDTO = new MatiereDTO() ;
        matiereDTO.setIntitule("nom test");
        List<MatiereDTO> matieres_list = List.of(matiereDTO);

        given(service.listMatiere()).willReturn(matieres_list) ;

        mvc.perform(get("/matiere")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].intitule", is(matiereDTO.getIntitule()))) ;
    }
}
