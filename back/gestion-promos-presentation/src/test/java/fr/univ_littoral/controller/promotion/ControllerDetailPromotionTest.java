package fr.univ_littoral.controller.promotion;

import fr.univ_littoral.controllers.note.DetailsNoteController;
import fr.univ_littoral.controllers.promotion.DetailsPromotionController;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DetailsPromotionController.class)
public class ControllerDetailPromotionTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private PromoServiceImpl service ;

    @Test
    public void DetailPromotion() throws Exception
    {
        PromoDTO promoDTO = new PromoDTO() ;
        promoDTO.setIntitule("intitule");
        promoDTO.setId(42);

        given(service.findPromoById(promoDTO.getId())).willReturn(promoDTO) ;

        mvc.perform(get("/promotion/"+promoDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
    }

    @Test
    public void should_throw_exception_when_promotion_doesnt_exist() throws Exception {
        PromoDTO promoDTO = new PromoDTO();
        promoDTO.setId(42);
        promoDTO.setIntitule("intitule");
        Mockito.doThrow(new Exception()).when(service).findPromoById(promoDTO.getId());
        mvc.perform(get("/promotion/" + promoDTO.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
