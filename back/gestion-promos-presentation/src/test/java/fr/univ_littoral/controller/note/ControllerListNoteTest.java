package fr.univ_littoral.controller.note;

import fr.univ_littoral.controllers.matiere.ListerMatiereController;
import fr.univ_littoral.controllers.note.ListerNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import fr.univ_littoral.service.note.NoteServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ListerNoteController.class)
public class ControllerListNoteTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private NoteServiceImpl service ;

    @Test
    public void listAllEtudiant() throws Exception
    {
        NoteDTO noteDTO = new NoteDTO() ;
        noteDTO.setNote(17);
        List<NoteDTO> notes_list = List.of(noteDTO);

        given(service.listNotes()).willReturn(notes_list) ;

        mvc.perform(get("/note")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].note", is(noteDTO.getNote()))) ;
    }
}
