package fr.univ_littoral.controller.matiere;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controller.JsonUtil;
import fr.univ_littoral.controllers.etudiant.CreerEtudiantController;
import fr.univ_littoral.controllers.matiere.CreerMatiereController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreerMatiereController.class)
public class ControllerCreerMatiereTest {
    @Autowired
    private MockMvc mockMvc ;

    @MockBean
    private MatiereServiceImpl service ;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createMatiere() throws Exception
    {
        MatiereDTO matiereDTO = new MatiereDTO() ;
        matiereDTO.setIntitule("Nom test");

        given(service.createMatiere(matiereDTO)).willReturn(matiereDTO) ;

        mockMvc.perform(post("/matiere")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(matiereDTO)))
                .andExpect(status().isCreated()) ;
    }
}
