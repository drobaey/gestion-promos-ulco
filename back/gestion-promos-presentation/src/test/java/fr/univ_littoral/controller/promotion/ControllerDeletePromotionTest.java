package fr.univ_littoral.controller.promotion;

import fr.univ_littoral.controllers.note.SupprimerNoteController;
import fr.univ_littoral.controllers.promotion.SupprimerPromotionController;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SupprimerPromotionController.class)
public class ControllerDeletePromotionTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private PromoServiceImpl service ;

    @Test
    public void removePromotion() throws Exception
    {
        PromoDTO promoDTO = new PromoDTO() ;
        promoDTO.setIntitule("intitule");
        promoDTO.setId(42);

        doNothing().when(service).deletePromoById(promoDTO.getId());

        mvc.perform(delete("/promotion/"+ promoDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent()) ;
    }

    @Test
    public void removeWhenPromoNotExist() throws Exception
    {
        PromoDTO promoDTO = new PromoDTO() ;
        promoDTO.setId(42);
        promoDTO.setIntitule("intitule");

        Mockito.doThrow(new Exception()).when(service).deletePromoById(promoDTO.getId());

        mvc.perform(delete("/promotion/"+promoDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}
