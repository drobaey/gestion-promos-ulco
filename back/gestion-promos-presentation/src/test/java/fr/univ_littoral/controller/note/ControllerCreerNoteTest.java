package fr.univ_littoral.controller.note;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controller.JsonUtil;
import fr.univ_littoral.controllers.matiere.CreerMatiereController;
import fr.univ_littoral.controllers.note.CreerNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import fr.univ_littoral.service.note.NoteServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreerNoteController.class)
public class ControllerCreerNoteTest {
    @Autowired
    private MockMvc mockMvc ;

    @MockBean
    private NoteServiceImpl service ;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createNote() throws Exception
    {
        NoteDTO noteDTO = new NoteDTO() ;
        noteDTO.setNote(17);

        given(service.createNote(noteDTO)).willReturn(noteDTO) ;

        mockMvc.perform(post("/note")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(noteDTO)))
                .andExpect(status().isCreated()) ;
    }
}
