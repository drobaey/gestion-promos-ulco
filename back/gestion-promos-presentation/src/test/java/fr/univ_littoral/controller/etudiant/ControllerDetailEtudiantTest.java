package fr.univ_littoral.controller.etudiant;

import fr.univ_littoral.controllers.etudiant.DetailEtudiantController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DetailEtudiantController.class)
public class ControllerDetailEtudiantTest {
    @Autowired
    private MockMvc mvc ;

    @MockBean
    private EtudiantServiceImpl service ;

    @Test
    public void DetailEtudiant() throws Exception
    {
        EtudiantDTO etudiantDTO = new EtudiantDTO() ;
        etudiantDTO.setNom("jon do");
        etudiantDTO.setId(42);

        given(service.findEtudiantById(etudiantDTO.getId())).willReturn(etudiantDTO) ;

        mvc.perform(get("/etudiant/"+etudiantDTO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isFound());
    }

    @Test
    public void should_throw_exception_when_etudiant_doesnt_exist() throws Exception {
        EtudiantDTO etudiantDTO = new EtudiantDTO();
        etudiantDTO.setId(42);
        etudiantDTO.setNom("Test Name");
        Mockito.doThrow(new Exception()).when(service).findEtudiantById(etudiantDTO.getId());
        mvc.perform(get("/etudiant/" + etudiantDTO.getId())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
