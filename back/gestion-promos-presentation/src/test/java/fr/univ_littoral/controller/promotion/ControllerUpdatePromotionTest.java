package fr.univ_littoral.controller.promotion;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controllers.note.ModifierNoteController;
import fr.univ_littoral.controllers.promotion.ModifierPromotionController;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ModifierPromotionController.class)
public class ControllerUpdatePromotionTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private PromoServiceImpl service;
    @Test
    public void updateUser_whenPutPromotion() throws Exception {
        PromoDTO promoDTO = new PromoDTO();
        promoDTO.setIntitule("intitule");
        promoDTO.setId(42);
        given(service.updatePromo(promoDTO)).willReturn(promoDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/promotion/"+ promoDTO.getId())
            .content(mapper.writeValueAsString(promoDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("intitule", is(promoDTO.getIntitule())));
    }
    @Test
    public void should_throw_exception_when_note_doesnt_exist() throws Exception {
        PromoDTO promoDTO = new PromoDTO();
        promoDTO.setId(42);
        promoDTO.setIntitule("intitule");
        Mockito.doThrow(new Exception()).when(service).updatePromo(promoDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/promotion/" + promoDTO.getId())
            .content(mapper.writeValueAsString(promoDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
