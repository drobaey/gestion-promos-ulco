package fr.univ_littoral.controller.matiere;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controllers.etudiant.ModifierEtudiantController;
import fr.univ_littoral.controllers.matiere.ModifierMatiereController;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ModifierMatiereController.class)
public class ControllerUpdateMatiereTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private MatiereServiceImpl service;
    @Test
    public void updateUser_whenPutMatiere() throws Exception {
        MatiereDTO matiereDTO = new MatiereDTO();
        matiereDTO.setIntitule("Test Name");
        matiereDTO.setId(42);
        given(service.updateMatiere(matiereDTO)).willReturn(matiereDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/etudiant"+ matiereDTO.getId())
            .content(mapper.writeValueAsString(matiereDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("nom", is(matiereDTO.getIntitule())));
    }
    @Test
    public void should_throw_exception_when_etudiant_doesnt_exist() throws Exception {
        MatiereDTO matiereDTO = new MatiereDTO();
        matiereDTO.setId(42);
        matiereDTO.setIntitule("Test name");
        Mockito.doThrow(new Exception()).when(service).updateMatiere(matiereDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/matiere/" + matiereDTO.getId())
            .content(mapper.writeValueAsString(matiereDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
