package fr.univ_littoral.controller.etudiant;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controllers.etudiant.CreerEtudiantController;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import fr.univ_littoral.controller.JsonUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CreerEtudiantController.class)
public class ControllerCreerEtudiantTest {
    @Autowired
    private MockMvc mockMvc ;

    @MockBean
    private EtudiantServiceImpl service ;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createEtudiant() throws Exception
    {
        EtudiantDTO etudiantDTO = new EtudiantDTO() ;
        etudiantDTO.setNom("Nom test");

        given(service.createEtudiant(etudiantDTO)).willReturn(etudiantDTO) ;

        mockMvc.perform(post("/etudiant")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonUtil.toJson(etudiantDTO)))
                .andExpect(status().isCreated()) ;
    }
}
