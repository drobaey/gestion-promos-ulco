package fr.univ_littoral.controller.note;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univ_littoral.controllers.matiere.ModifierMatiereController;
import fr.univ_littoral.controllers.note.ModifierNoteController;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import fr.univ_littoral.service.note.NoteServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ModifierNoteController.class)
public class ControllerUpdateNoteTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private NoteServiceImpl service;
    @Test
    public void updateUser_whenPutNote() throws Exception {
        NoteDTO noteDTO = new NoteDTO();
        noteDTO.setNote(17);
        noteDTO.setId(42);
        given(service.updateNote(noteDTO)).willReturn(noteDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/note"+ noteDTO.getId())
            .content(mapper.writeValueAsString(noteDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("nom", is(noteDTO.getNote())));
    }
    @Test
    public void should_throw_exception_when_note_doesnt_exist() throws Exception {
        NoteDTO noteDTO = new NoteDTO();
        noteDTO.setId(42);
        noteDTO.setNote(17);
        Mockito.doThrow(new Exception()).when(service).updateNote(noteDTO);
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(put("/note/" + noteDTO.getId())
            .content(mapper.writeValueAsString(noteDTO))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }
}
