package fr.univ_littoral.data;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Table(name = "matiere")
public class Matiere implements Serializable {
    @Serial
    private static final long serialVersionUID = 2L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "id_promotion")
    private int idPromotion;
    @Basic
    @Column(name = "intitule")
    private String intitule;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Matiere(int idPromotion, String intitule) {
        this.idPromotion = idPromotion;
        this.intitule = intitule;
    }

    public Matiere() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Matiere matiere = (Matiere) o;

        if (id != matiere.id) return false;
        if (idPromotion != matiere.idPromotion) return false;
        if (intitule != null ? !intitule.equals(matiere.intitule) : matiere.intitule != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idPromotion;
        result = 31 * result + (intitule != null ? intitule.hashCode() : 0);
        return result;
    }
}