package fr.univ_littoral.data;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "etudiant")
public class Etudiant implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "id_promotion")
    private int idPromotion;
    @Basic
    @Column(name = "prenom")
    private String prenom;
    @Basic
    @Column(name = "nom")
    private String nom;
    @Basic
    @Column(name = "numero_etudiant")
    private int numeroEtudiant;
    @Basic
    @Column(name = "numero_INE")
    private String numeroIne;
    @Basic
    @Column(name = "numero_lecteur")
    private int numeroLecteur;
    @Basic
    @Column(name = "date_naissance")
    private Date dateNaissance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPromotion() {
        return idPromotion;
    }

    public void setIdPromotion(int idPromotion) {
        this.idPromotion = idPromotion;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumeroEtudiant() {
        return numeroEtudiant;
    }

    public void setNumeroEtudiant(int numeroEtudiant) {
        this.numeroEtudiant = numeroEtudiant;
    }

    public String getNumeroIne() {
        return numeroIne;
    }

    public void setNumeroIne(String numeroIne) {
        this.numeroIne = numeroIne;
    }

    public int getNumeroLecteur() {
        return numeroLecteur;
    }

    public void setNumeroLecteur(int numeroLecteur) {
        this.numeroLecteur = numeroLecteur;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Etudiant(int idPromotion, String nom, String prenom, int numeroEtudiant, String numeroIne,
                    Date dateNaissance, int numeroLecteur) {
        this.idPromotion = idPromotion;
        this.nom = nom;
        this.prenom = prenom;
        this.numeroEtudiant = numeroEtudiant;
        this.numeroIne = numeroIne;
        this.dateNaissance = dateNaissance;
        this.numeroLecteur = numeroLecteur;
    }

    public Etudiant() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Etudiant etudiant = (Etudiant) o;

        if (id != etudiant.id) return false;
        if (idPromotion != etudiant.idPromotion) return false;
        if (numeroEtudiant != etudiant.numeroEtudiant) return false;
        if (numeroLecteur != etudiant.numeroLecteur) return false;
        if (prenom != null ? !prenom.equals(etudiant.prenom) : etudiant.prenom != null) return false;
        if (nom != null ? !nom.equals(etudiant.nom) : etudiant.nom != null) return false;
        if (numeroIne != null ? !numeroIne.equals(etudiant.numeroIne) : etudiant.numeroIne != null) return false;
        if (dateNaissance != null ? !dateNaissance.equals(etudiant.dateNaissance) : etudiant.dateNaissance != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idPromotion;
        result = 31 * result + (prenom != null ? prenom.hashCode() : 0);
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + numeroEtudiant;
        result = 31 * result + (numeroIne != null ? numeroIne.hashCode() : 0);
        result = 31 * result + numeroLecteur;
        result = 31 * result + (dateNaissance != null ? dateNaissance.hashCode() : 0);
        return result;
    }
}
