package fr.univ_littoral.data;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;

@Entity
@Table(name = "note")
public class Note implements Serializable {
    @Serial
    private static final long serialVersionUID = 3L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "note")
    private double note;
    @Basic
    @Column(name = "id_etudiant")
    private int idEtudiant;
    @Basic
    @Column(name = "id_matiere")
    private int idMatiere;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public int getIdMatiere() {
        return idMatiere;
    }

    public void setIdMatiere(int idMatiere) {
        this.idMatiere = idMatiere;
    }

    public Note(double note, int idEtudiant, int idMatiere) {
        this.note = note;
        this.idEtudiant = idEtudiant;
        this.idMatiere = idMatiere;
    }

    public Note() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Note notes = (Note) o;

        if (id != notes.id) return false;
        if (Double.compare(notes.note, note) != 0) return false;
        if (idEtudiant != notes.idEtudiant) return false;
        if (idMatiere != notes.idMatiere) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(note);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + idEtudiant;
        result = 31 * result + idMatiere;
        return result;
    }
}
