package fr.univ_littoral.data;

import jakarta.persistence.*;

import java.io.Serial;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "promotion")
public class Promotion implements Serializable {
    @Serial
    private static final long serialVersionUID = 4L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private int id;
    @Basic
    @Column(name = "intitule")
    private String intitule;
    @Basic
    @Column(name = "annee")
    private int annee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public Promotion(String intitule, int annee) {
        this.intitule = intitule;
        this.annee = annee;
    }

    public Promotion() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Promotion that = (Promotion) o;

        if (id != that.id) return false;
        if (intitule != null ? !intitule.equals(that.intitule) : that.intitule != null) return false;
        if (annee != that.annee) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (intitule != null ? intitule.hashCode() : 0);
        result = 31 * result + annee;
        return result;
    }
}