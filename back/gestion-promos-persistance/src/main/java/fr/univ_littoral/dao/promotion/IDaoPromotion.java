package fr.univ_littoral.dao.promotion;

import fr.univ_littoral.data.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoPromotion extends JpaRepository<Promotion, Integer> {

}
