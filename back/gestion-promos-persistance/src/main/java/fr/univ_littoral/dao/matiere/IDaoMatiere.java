package fr.univ_littoral.dao.matiere;

import fr.univ_littoral.data.Matiere;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoMatiere extends JpaRepository<Matiere, Integer> {

}
