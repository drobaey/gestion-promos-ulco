package fr.univ_littoral.dao.note;

import fr.univ_littoral.data.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDaoNote extends JpaRepository<Note, Integer> {

}
