package fr.univ_littoral.dao.etudiant;

import fr.univ_littoral.data.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IDaoEtudiant extends JpaRepository<Etudiant, Integer> {
    @Query(DaoEtudiantQuerries.FIND_BY_PROMO_ID)
    public List<Etudiant> findByIdPromo(int idPromo);
}