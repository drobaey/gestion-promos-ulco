CREATE TABLE IF NOT EXISTS promotion (
    id INT AUTO_INCREMENT PRIMARY KEY,
    intitule VARCHAR(100) NOT NULL,
    annee INT NOT NULL
);

CREATE TABLE IF NOT EXISTS etudiant (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_promotion INT NOT NULL,
    prenom VARCHAR(100) NOT NULL,
    nom VARCHAR(100) NOT NULL,
    numero_etudiant INT NOT NULL,
    numero_INE VARCHAR(100) NOT NULL,
    numero_lecteur INT NOT NULL,
    date_naissance DATE NOT NULL,
    FOREIGN KEY (id_promotion) REFERENCES promotion(id)
);

CREATE TABLE iF NOT EXISTS matiere (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_promotion INT NOT NULL,
    intitule VARCHAR(100) NOT NULL,
    FOREIGN KEY (id_promotion) REFERENCES promotion(id)
);

CREATE TABLE IF NOT EXISTS note (
    id INT AUTO_INCREMENT PRIMARY KEY,
    note FLOAT NOT NULL,
    id_etudiant INT NOT NULL,
    id_matiere INT NOT NULL,
    FOREIGN KEY (id_etudiant) REFERENCES etudiant(id),
    FOREIGN KEY (id_matiere) REFERENCES matiere(id)
);
