package fr.univ_littoral.service.etudiant;

import fr.univ_littoral.dao.etudiant.IDaoEtudiant;
import fr.univ_littoral.dto.EtudiantDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface IEtudiantService {
    public List<EtudiantDTO> listEtudiants();

    public EtudiantDTO findEtudiantById(int id);

    public EtudiantDTO createEtudiant(EtudiantDTO etu_dto);

    public void deleteEtudiantById(int id);
    
    public void setDaoEtudiant(IDaoEtudiant daoEtudiant);
    public EtudiantDTO updateEtudiant(EtudiantDTO etu_dto) ;
}