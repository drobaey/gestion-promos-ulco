package fr.univ_littoral.service.promotion;

import fr.univ_littoral.dao.promotion.IDaoPromotion;
import fr.univ_littoral.dto.PromoDTO;

import java.util.List;

public interface IPromoService {
    public List<PromoDTO> listPromos() ;
    public PromoDTO findPromoById(int id) ;
    public PromoDTO createPromo(PromoDTO promo_dto) ;
    public void deletePromoById(int id_promo) ;
    public PromoDTO updatePromo(PromoDTO promo_dto) ;
    public void setDaoPromo(IDaoPromotion daopromo);

}
