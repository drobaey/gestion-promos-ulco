package fr.univ_littoral.service.matiere;

import fr.univ_littoral.dao.matiere.IDaoMatiere;
import fr.univ_littoral.dto.MatiereDTO;

import java.util.List;

public interface IMatiereService {
    public List<MatiereDTO> listMatiere() ;
    public MatiereDTO findMatiereById(int id) ;
    public MatiereDTO createMatiere(MatiereDTO mat_dto) ;

    public MatiereDTO updateMatiere(MatiereDTO mat_dto);
    public void deleteMatiereById(int id) ;
    public void setDaoMatieres(IDaoMatiere daoMatieres);
}
