package fr.univ_littoral.service.matiere;

import fr.univ_littoral.convert.MatiereConvert;
import fr.univ_littoral.dao.matiere.IDaoMatiere;
import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.dto.MatiereDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MatiereServiceImpl implements IMatiereService {
    private IDaoMatiere daoMatieres;

    // add other services here

    public List<MatiereDTO> listMatiere() {
        List<MatiereDTO> matiere_dto_list = new ArrayList<>() ;
        List<Matiere> matiere_list = daoMatieres.findAll() ;
        for(Matiere matiere : matiere_list) {
            matiere_dto_list.add(MatiereConvert.getInstance().convertEntityToDto(matiere)) ;
        }
        return matiere_dto_list ;
    }

    public MatiereDTO findMatiereById(int id) {
        Optional<Matiere> matiere_trouve = daoMatieres.findById(id) ;

        if(matiere_trouve.isPresent()) {
            return MatiereConvert.getInstance().convertEntityToDto(matiere_trouve.get()) ;
        }

        return null;
    }

    public MatiereDTO createMatiere(MatiereDTO mat_dto) {
        return MatiereConvert.getInstance().convertEntityToDto(
            daoMatieres.save(MatiereConvert.getInstance().convertDtoToEntity(mat_dto))
        );
    }

    public void deleteMatiereById(int id) {
        daoMatieres.deleteById(id);
    }

    public MatiereDTO updateMatiere(MatiereDTO mat_dto) {
        Matiere mat_to_update = daoMatieres.getReferenceById(mat_dto.getId()) ;

        mat_to_update.setIntitule(mat_dto.getIntitule());
        mat_to_update.setIdPromotion(mat_dto.getId_promotion());

        daoMatieres.save(mat_to_update) ;

        return MatiereConvert.getInstance().convertEntityToDto(mat_to_update) ;
    }

    @Autowired
    public void setDaoMatieres(IDaoMatiere daoMatieres) {
        this.daoMatieres = daoMatieres;
    }
}
