package fr.univ_littoral.service.note;

import fr.univ_littoral.dao.note.IDaoNote;
import fr.univ_littoral.dto.NoteDTO;

import java.util.List;

public interface INoteService {
    public List<NoteDTO> listNotes() ;
    public NoteDTO findNoteById(int id) ;
    public NoteDTO createNote(NoteDTO note_dto) ;
    public void deleteNoteById(int id_note) ;
    public NoteDTO updateNote(NoteDTO note_dto) ;
    public void setDaoNote(IDaoNote daoNote);

}
