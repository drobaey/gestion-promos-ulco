package fr.univ_littoral.service.promotion;

import fr.univ_littoral.convert.PromoConvert;
import fr.univ_littoral.dao.promotion.IDaoPromotion;
import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.data.Promotion;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.promotion.IPromoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PromoServiceImpl implements IPromoService {
    private IDaoPromotion daoPromo;

    public List<PromoDTO> listPromos() {
        List<PromoDTO> promo_dto_list = new ArrayList<>() ;
        List<Promotion> matiere_list = daoPromo.findAll() ;
        for(Promotion promotion : matiere_list) {
            promo_dto_list.add(PromoConvert.getInstance().convertEntityToDto(promotion)) ;
        }
        return promo_dto_list ;
    }

    public PromoDTO findPromoById(int id) {
        Optional<Promotion> promo_trouve = daoPromo.findById(id) ;
        if(promo_trouve.isPresent()) {
            return PromoConvert.getInstance().convertEntityToDto(promo_trouve.get()) ;
        }
        return new PromoDTO() ;
    }

    public PromoDTO createPromo(PromoDTO promo_dto) {
        return PromoConvert.getInstance().convertEntityToDto(
            daoPromo.save(PromoConvert.getInstance().convertDtoToEntity(promo_dto))
        );
    }

    public void deletePromoById(int id_promo) {
        daoPromo.deleteById(id_promo);
    }

    public PromoDTO updatePromo(PromoDTO promo_dto) {
        Promotion promo_to_update = daoPromo.getReferenceById(promo_dto.getId()) ;

        promo_to_update.setIntitule(promo_dto.getIntitule());
        promo_to_update.setAnnee(promo_dto.getAnnee());

        daoPromo.save(promo_to_update) ;

        return PromoConvert.getInstance().convertEntityToDto(promo_to_update);
    }

    @Autowired
    public void setDaoPromo(IDaoPromotion daopromo) {
        this.daoPromo = daopromo;
    }
}
