package fr.univ_littoral.service.note;

import fr.univ_littoral.convert.NoteConvert;
import fr.univ_littoral.dao.note.IDaoNote;
import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.data.Note;
import fr.univ_littoral.dto.NoteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NoteServiceImpl implements INoteService {
    private IDaoNote daoNote;

    // add other services here

    public List<NoteDTO> listNotes() {
        List<NoteDTO> notes_dto_list = new ArrayList<>() ;
        List<Note> note_list = daoNote.findAll() ;
        for(Note note : note_list) {
            notes_dto_list.add(NoteConvert.getInstance().convertEntityToDto(note)) ;
        }
        return notes_dto_list ;
    }

    public NoteDTO findNoteById(int id) {
        Optional<Note> note_trouve = daoNote.findById(id) ;
        if(note_trouve.isPresent()) {
            return NoteConvert.getInstance().convertEntityToDto(note_trouve.get()) ;
        }
        return new NoteDTO() ;
    }

    public NoteDTO createNote(NoteDTO note_dto) {
        return NoteConvert.getInstance().convertEntityToDto(
            daoNote.save(NoteConvert.getInstance().convertDtoToEntity(note_dto))
        );
    }

    public void deleteNoteById(int id_note) {
        daoNote.deleteById(id_note);
    }

    public NoteDTO updateNote(NoteDTO note_dto) {
        Note note_to_update = daoNote.getReferenceById(note_dto.getId()) ;

        note_to_update.setNote(note_dto.getNote());
        note_to_update.setIdEtudiant(note_dto.getId_etudiant());
        note_to_update.setIdMatiere(note_dto.getId_matiere());

        daoNote.save(note_to_update) ;

        return NoteConvert.getInstance().convertEntityToDto(note_to_update);
    }

    @Autowired
    public void setDaoNote(IDaoNote daoNote) {
        this.daoNote = daoNote;
    }
}
