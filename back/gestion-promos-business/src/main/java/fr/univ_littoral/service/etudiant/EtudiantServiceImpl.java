package fr.univ_littoral.service.etudiant;

import fr.univ_littoral.convert.EtudiantConvert;
import fr.univ_littoral.convert.MatiereConvert;
import fr.univ_littoral.dao.etudiant.IDaoEtudiant;
import fr.univ_littoral.dao.matiere.IDaoMatiere;
import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.dto.EtudiantDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EtudiantServiceImpl implements IEtudiantService {
    private IDaoEtudiant daoEtudiant;

    public List<EtudiantDTO> listEtudiants() {
        List<EtudiantDTO> etudiant_dto_list = new ArrayList<>() ;
        List<Etudiant> etudiant_list = daoEtudiant.findAll() ;
        for(Etudiant etudiant : etudiant_list) {
            etudiant_dto_list.add(EtudiantConvert.getInstance().convertEntityToDto(etudiant)) ;
        }
        return etudiant_dto_list ;
    }

    public EtudiantDTO findEtudiantById(int id) {
        Optional<Etudiant> etudiant_trouve = daoEtudiant.findById(id) ;
        if(etudiant_trouve.isPresent()) {
            return EtudiantConvert.getInstance().convertEntityToDto(etudiant_trouve.get()) ;
        }
        return new EtudiantDTO() ;
    }

    public EtudiantDTO createEtudiant(EtudiantDTO etu_dto) {
        return EtudiantConvert.getInstance().convertEntityToDto(
            daoEtudiant.save(EtudiantConvert.getInstance().convertDtoToEntity(etu_dto))
        );
    }

    public void deleteEtudiant(EtudiantDTO etu_dto) {
        daoEtudiant.delete(EtudiantConvert.getInstance().convertDtoToEntity(etu_dto));
    }

    public void deleteEtudiantById(int id) {
        daoEtudiant.deleteById(id);
    }

    public EtudiantDTO updateEtudiant(EtudiantDTO etu_dto) {
        Etudiant etu_to_update = daoEtudiant.getReferenceById(etu_dto.getId()) ;

        //etu_to_update.setId(etu_dto.getId());
        etu_to_update.setNom(etu_dto.getNom());
        etu_to_update.setPrenom(etu_dto.getPrenom());
        etu_to_update.setNumeroLecteur(etu_dto.getNumero_lecteur());
        etu_to_update.setNumeroEtudiant(etu_dto.getNumero_etudiant());
        etu_to_update.setNumeroIne(etu_dto.getNumero_INE());
        etu_to_update.setDateNaissance(etu_dto.getDate_naissance());
        etu_to_update.setIdPromotion(etu_dto.getId_promotion());

        daoEtudiant.save(etu_to_update) ;

        return EtudiantConvert.getInstance().convertEntityToDto(etu_to_update);
    }

    @Autowired
    public void setDaoEtudiant(IDaoEtudiant daoEtudiant) {
        this.daoEtudiant = daoEtudiant;
    }

}
