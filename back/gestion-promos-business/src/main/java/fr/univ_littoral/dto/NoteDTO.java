package fr.univ_littoral.dto;

import java.io.Serializable;

public class NoteDTO implements Serializable {

        /* attributes */
    private int id ;
    private double note ;
    private int id_etudiant ;
    private int id_matiere ;


        /* setter */
    public void setId(int id) {
        this.id = id;
    }

    public void setId_etudiant(int id_etudiant) {
        this.id_etudiant = id_etudiant;
    }

    public void setId_matiere(int id_matiere) {
        this.id_matiere = id_matiere;
    }

    public void setNote(double note) {
        this.note = note;
    }

        /* getter */

    public int getId() {
        return id;
    }

    public double getNote() {
        return note;
    }

    public int getId_etudiant() {
        return id_etudiant;
    }

    public int getId_matiere() {
        return id_matiere;
    }
}
