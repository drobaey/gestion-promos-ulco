package fr.univ_littoral.dto;

import java.io.Serializable;
import java.sql.Date;

public class EtudiantDTO implements Serializable {

        /* attributes */

    private int id;
    private int id_promotion;
    private String prenom;
    private String nom;
    private int numero_etudiant;
    private String numero_INE;
    private int numero_lecteur;
    private Date date_naissance;


        /* setter */

    public void setId(int id) {
        this.id = id;
    }

    public void setDate_naissance(Date date_naissance) {
        this.date_naissance = date_naissance;
    }

    public void setId_promotion(int id_promotion) {
        this.id_promotion = id_promotion;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setNumero_etudiant(int numero_etudiant) {
        this.numero_etudiant = numero_etudiant;
    }

    public void setNumero_INE(String numero_INE) {
        this.numero_INE = numero_INE;
    }

    public void setNumero_lecteur(int numero_lecteur) {
        this.numero_lecteur = numero_lecteur;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


        /* getter */
    public int getId() {
        return id;
    }

    public Date getDate_naissance() {
        return date_naissance;
    }

    public int getId_promotion() {
        return id_promotion;
    }

    public int getNumero_etudiant() {
        return numero_etudiant;
    }

    public int getNumero_lecteur() {
        return numero_lecteur;
    }

    public String getNom() {
        return nom;
    }

    public String getNumero_INE() {
        return numero_INE;
    }

    public String getPrenom() {
        return prenom;
    }
}
