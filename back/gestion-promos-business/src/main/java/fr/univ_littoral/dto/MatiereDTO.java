package fr.univ_littoral.dto;

import java.io.Serializable;

public class MatiereDTO implements Serializable {

        /* attributs */
    private int id;
    private int id_promotion;
    private String intitule;


        /* getter */
    public int getId() {
        return id;
    }

    public int getId_promotion() {
        return id_promotion;
    }

    public String getIntitule() {
        return intitule;
    }

        /* setter */

    public void setId(int id) {
        this.id = id;
    }

    public void setId_promotion(int id_promotion) {
        this.id_promotion = id_promotion;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }
}
