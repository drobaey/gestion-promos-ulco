package fr.univ_littoral.dto;

import java.io.Serializable;

public class PromoDTO implements Serializable {

    /* attributes */
    private int id;
    private String intitule;
    private int annee;


        /* setter */
    public void setId(int id) {
        this.id = id;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }


        /* getter */

    public int getId() {
        return id;
    }

    public String getIntitule() {
        return intitule;
    }

    public int getAnnee() {
        return annee;
    }
}