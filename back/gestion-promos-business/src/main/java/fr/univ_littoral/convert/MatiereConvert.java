package fr.univ_littoral.convert;

import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.dto.MatiereDTO;

import java.util.ArrayList;

public final class MatiereConvert {

        /* singleton pattern */
    private static MatiereConvert instance ;
    private MatiereConvert() {}
    public static MatiereConvert getInstance() {
        if(instance == null) {
            return new MatiereConvert() ;
        } else {
            return instance ;
        }
    }

        /* convert methods */
    public Matiere convertDtoToEntity(final MatiereDTO matiere_dto) {
        final Matiere matieres = new Matiere();

        if(matiere_dto != null) {
            matieres.setId(matiere_dto.getId());
            matieres.setIntitule(matiere_dto.getIntitule());
            matieres.setIdPromotion(matiere_dto.getId_promotion());
        }
        return matieres ;
    }

    public MatiereDTO convertEntityToDto(final Matiere matieres){
        final MatiereDTO matiere_dto = new MatiereDTO() ;
        if(matieres != null) {
            matiere_dto.setId(matieres.getId());
            matiere_dto.setIntitule(matieres.getIntitule());
            matiere_dto.setId_promotion(matieres.getIdPromotion());
        }
        return matiere_dto ;
    }

    public ArrayList<Matiere> convertDtoListToEntityList(ArrayList<MatiereDTO> dto_list) {
        ArrayList<Matiere> mat_list = new ArrayList<Matiere>();
        for(MatiereDTO etu_dto : dto_list) {
            mat_list.add(convertDtoToEntity(etu_dto)) ;
        }

        return mat_list ;
    }

    public ArrayList<MatiereDTO> convertEntityListToDtoList(ArrayList<Matiere> mat_list) {
        ArrayList<MatiereDTO> dto_list = new ArrayList<>();
        for(Matiere mat : mat_list) {
            dto_list.add(convertEntityToDto(mat)) ;
        }

        return dto_list ;
    }

}