package fr.univ_littoral.convert;

import fr.univ_littoral.data.Promotion;
import fr.univ_littoral.dto.PromoDTO;

import java.util.ArrayList;

public final class PromoConvert {

        /* singleton pattern */
    private static PromoConvert instance ;
    private PromoConvert() {}
    public static PromoConvert getInstance() {
        if(instance == null) {
            return new PromoConvert() ;
        } else {
            return instance ;
        }
    }

        /* convert methods */
    public Promotion convertDtoToEntity(final PromoDTO promo_dto){
        final Promotion promos = new Promotion() ;
        if(promo_dto != null) {
            promos.setId(promo_dto.getId());
            promos.setAnnee(promo_dto.getAnnee());
            promos.setIntitule(promo_dto.getIntitule());
        }
        return promos ;
    }

    public PromoDTO convertEntityToDto(final Promotion notes){
        final PromoDTO promo_dto = new PromoDTO() ;
        if(notes != null) {
            promo_dto.setId(notes.getId());
            promo_dto.setAnnee(notes.getAnnee());
            promo_dto.setIntitule(notes.getIntitule());
        }
        return promo_dto ;
    }

    public ArrayList<Promotion> convertDtoListToEntityList(ArrayList<PromoDTO> dto_list) {
        ArrayList<Promotion> note_list = new ArrayList<Promotion>();
        for(PromoDTO note_dto : dto_list) {
            note_list.add(convertDtoToEntity(note_dto)) ;
        }

        return note_list ;
    }

    public ArrayList<PromoDTO> convertEntityListToDtoList(ArrayList<Promotion> note_list) {
        ArrayList<PromoDTO> dto_list = new ArrayList<>();
        for(Promotion note : note_list) {
            dto_list.add(convertEntityToDto(note)) ;
        }

        return dto_list ;
    }

}