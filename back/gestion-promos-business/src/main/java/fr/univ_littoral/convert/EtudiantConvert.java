package fr.univ_littoral.convert;

import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.dto.EtudiantDTO;

import java.util.ArrayList;

public final class EtudiantConvert {

        /* singleton pattern */
    private static EtudiantConvert instance ;
    private EtudiantConvert () {}
    public static EtudiantConvert getInstance() {
        if(instance == null) {
            return new EtudiantConvert() ;
        } else {
            return instance ;
        }
    }

        /* convert methods */
    public Etudiant convertDtoToEntity(final EtudiantDTO etu_dto){
        final Etudiant etudiant = new Etudiant() ;
        if(etu_dto != null) {
            etudiant.setId(etu_dto.getId());
            etudiant.setDateNaissance(etu_dto.getDate_naissance());
            etudiant.setNumeroEtudiant(etu_dto.getNumero_etudiant());
            etudiant.setNom(etu_dto.getNom());
            etudiant.setPrenom(etu_dto.getPrenom());
            etudiant.setIdPromotion(etu_dto.getId_promotion());
            etudiant.setNumeroIne(etu_dto.getNumero_INE());
            etudiant.setNumeroLecteur(etu_dto.getNumero_lecteur());
        }
        return etudiant ;
    }

    public EtudiantDTO convertEntityToDto(final Etudiant etu){
        final EtudiantDTO etu_dto = new EtudiantDTO() ;
        if(etu != null) {
            etu_dto.setId(etu.getId());
            etu_dto.setDate_naissance(etu.getDateNaissance());
            etu_dto.setNumero_etudiant(etu.getNumeroEtudiant());
            etu_dto.setNom(etu.getNom());
            etu_dto.setPrenom(etu.getPrenom());
            etu_dto.setId_promotion(etu.getIdPromotion());
            etu_dto.setNumero_INE(etu.getNumeroIne());
            etu_dto.setNumero_lecteur(etu.getNumeroLecteur());
        }
        return etu_dto ;
    }

    public ArrayList<Etudiant> convertDtoListToEntityList(ArrayList<EtudiantDTO> dto_list) {
        ArrayList<Etudiant> note_list = new ArrayList<Etudiant>();
        for(EtudiantDTO note_dto : dto_list) {
            note_list.add(convertDtoToEntity(note_dto)) ;
        }

        return note_list ;
    }

    public ArrayList<EtudiantDTO> convertEntityListToDtoList(ArrayList<Etudiant> note_list) {
        ArrayList<EtudiantDTO> dto_list = new ArrayList<>();
        for(Etudiant note : note_list) {
            dto_list.add(convertEntityToDto(note)) ;
        }

        return dto_list ;
    }

}