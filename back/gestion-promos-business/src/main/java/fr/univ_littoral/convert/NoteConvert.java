package fr.univ_littoral.convert;

import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.data.Note;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.dto.NoteDTO;

import java.util.ArrayList;

public final class NoteConvert {

        /* singleton pattern */
    private static NoteConvert instance ;
    private NoteConvert() {}
    public static NoteConvert getInstance() {
        if(instance == null) {
            return new NoteConvert() ;
        } else {
            return instance ;
        }
    }

        /* convert methods */
    public Note convertDtoToEntity(final NoteDTO note_dto){
        final Note notes = new Note();

        if(note_dto != null) {
            notes.setId(note_dto.getId());
            notes.setNote(note_dto.getNote());
            notes.setIdEtudiant(note_dto.getId_etudiant());
            notes.setIdMatiere(note_dto.getId_matiere());
        }
        return notes ;
    }

    public NoteDTO convertEntityToDto(final Note notes){
        final NoteDTO note_dto = new NoteDTO() ;
        if(notes != null) {
            note_dto.setId(notes.getId());
            note_dto.setNote(notes.getNote());
            note_dto.setId_etudiant(notes.getIdEtudiant());
            note_dto.setId_matiere(notes.getIdMatiere());
        }
        return note_dto ;
    }

    public ArrayList<Note> convertDtoListToEntityList(ArrayList<NoteDTO> dto_list) {
        ArrayList<Note> note_list = new ArrayList<Note>();
        for(NoteDTO note_dto : dto_list) {
            note_list.add(convertDtoToEntity(note_dto)) ;
        }

        return note_list ;
    }

    public ArrayList<NoteDTO> convertEntityListToDtoList(ArrayList<Note> note_list) {
        ArrayList<NoteDTO> dto_list = new ArrayList<>();
        for(Note note : note_list) {
            dto_list.add(convertEntityToDto(note)) ;
        }

        return dto_list ;
    }

}