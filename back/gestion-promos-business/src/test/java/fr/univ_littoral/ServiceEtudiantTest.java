package fr.univ_littoral;

import fr.univ_littoral.convert.EtudiantConvert;
import fr.univ_littoral.dao.etudiant.IDaoEtudiant;
import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.service.etudiant.EtudiantServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals ;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceEtudiantTest {

    @Mock
    private IDaoEtudiant dao ;

    @InjectMocks
    private EtudiantServiceImpl etudiantService;

    @Test
    public void createEtudiantTest() {
        Etudiant etudiant = new Etudiant() ;
        etudiant.setNom("NameTest");
        when(dao.save(ArgumentMatchers.any(Etudiant.class))).thenReturn(etudiant) ;

        EtudiantDTO created = etudiantService.createEtudiant(EtudiantConvert.getInstance().convertEntityToDto(etudiant));
        assertEquals(created.getNom(), etudiant.getNom()) ;
        verify(dao).save(etudiant) ;
    }

    @Test
    public void getAllEtudiantTest() {
        ArrayList<Etudiant> etudiants_list = new ArrayList<>();
        etudiants_list.add(new Etudiant()) ;
        when(dao.findAll()).thenReturn(etudiants_list) ;

        List<EtudiantDTO> expected = etudiantService.listEtudiants();
        List<Etudiant> ex = EtudiantConvert.getInstance().convertDtoListToEntityList(new ArrayList<>(expected));
        assertEquals(ex, etudiants_list);
        verify(dao).findAll() ;
    }

    @Test
    public void getOneEtudiantTest() {
        Etudiant etudiant = new Etudiant() ;
        etudiant.setNom("nameTest");

        when(dao.findById(etudiant.getId())).thenReturn(Optional.of(etudiant));

        EtudiantDTO res = etudiantService.findEtudiantById(etudiant.getId()) ;

        assertEquals(res.getNom(), etudiant.getNom());
    }

    @Test
    public void deleteEtudiantByIdTest() {
        Etudiant etudiant = new Etudiant() ;
        etudiant.setId(42);

        etudiantService.deleteEtudiantById(etudiant.getId());
        verify(dao).deleteById(etudiant.getId());
    }

    @Test
    public void updateEtudiantTest() {
        Etudiant etudiant = new Etudiant() ;
        etudiant.setId(13);
        etudiant.setNom("NameTest");
        etudiantService.createEtudiant(EtudiantConvert.getInstance().convertEntityToDto(etudiant)) ;

        Etudiant updated_etudiant = new Etudiant() ;
        updated_etudiant.setNom("new NameTest");
        updated_etudiant.setId(13);

        when(dao.getReferenceById(etudiant.getId())).thenReturn(etudiant) ;
        when(dao.findById(etudiant.getId())).thenReturn(Optional.of(etudiant)) ;

        // verify update
        etudiantService.updateEtudiant(
                EtudiantConvert.getInstance().convertEntityToDto(updated_etudiant)
        );
        verify(dao).save(updated_etudiant) ;
        verify(dao).getReferenceById(etudiant.getId()) ;

        // assert new value
        EtudiantDTO res = etudiantService.findEtudiantById(etudiant.getId()) ;
        assertEquals(updated_etudiant.getNom(), res.getNom());
    }
}
