package fr.univ_littoral;

import fr.univ_littoral.convert.MatiereConvert;
import fr.univ_littoral.dao.matiere.IDaoMatiere;
import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceMatiereTest {

    @Mock
    private IDaoMatiere dao ;

    @InjectMocks
    private MatiereServiceImpl matiereService;

    @Test
    public void createMatiereTest() {
        Matiere matiere = new Matiere() ;
        matiere.setIntitule("matiereTest");
        when(dao.save(ArgumentMatchers.any(Matiere.class))).thenReturn(matiere) ;

        MatiereDTO created = matiereService.createMatiere(MatiereConvert.getInstance().convertEntityToDto(matiere));
        assertEquals(created.getIntitule(), matiere.getIntitule()) ;
        verify(dao).save(matiere) ;
    }

    @Test
    public void getAllMatiereTest() {
        ArrayList<Matiere> matieres_list = new ArrayList<>();
        matieres_list.add(new Matiere()) ;
        when(dao.findAll()).thenReturn(matieres_list) ;

        List<MatiereDTO> expected = matiereService.listMatiere();
        List<Matiere> ex = MatiereConvert.getInstance().convertDtoListToEntityList(new ArrayList<>(expected));
        assertEquals(ex, matieres_list);
        verify(dao).findAll() ;
    }

    @Test
    public void getOneMatiereTest() {
        Matiere matiere = new Matiere() ;
        matiere.setIntitule("matiereTest");

        when(dao.findById(matiere.getId())).thenReturn(Optional.of(matiere));

        MatiereDTO res = matiereService.findMatiereById(matiere.getId()) ;

        assertEquals(res.getIntitule(), matiere.getIntitule());
    }

    @Test
    public void deleteMatiereByIdTest() {
        Matiere matiere = new Matiere() ;
        matiere.setId(42);

        matiereService.deleteMatiereById(matiere.getId());
        verify(dao).deleteById(matiere.getId());
    }

    @Test
    public void updateMatiereTest() {
        Matiere matiere = new Matiere() ;
        matiere.setId(13);
        matiere.setIntitule("matiereTest");
        matiereService.createMatiere(MatiereConvert.getInstance().convertEntityToDto(matiere)) ;

        Matiere updated_matiere = new Matiere() ;
        updated_matiere.setIntitule("new matiereTest");
        updated_matiere.setId(13);

        when(dao.getReferenceById(matiere.getId())).thenReturn(matiere) ;
        when(dao.findById(matiere.getId())).thenReturn(Optional.of(matiere)) ;

            // verify update
        matiereService.updateMatiere(
                MatiereConvert.getInstance().convertEntityToDto(updated_matiere)
        );
        verify(dao).save(updated_matiere) ;
        verify(dao).getReferenceById(matiere.getId()) ;

            // assert new value
        MatiereDTO res = matiereService.findMatiereById(matiere.getId()) ;
        assertEquals(updated_matiere.getIntitule(), res.getIntitule());
    }
}
