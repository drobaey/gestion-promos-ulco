package fr.univ_littoral;

import fr.univ_littoral.convert.NoteConvert;
import fr.univ_littoral.dao.note.IDaoNote;
import fr.univ_littoral.dao.promotion.IDaoPromotion;
import fr.univ_littoral.data.Note;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.service.note.NoteServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServiceNoteTest {

    @Mock
    private IDaoNote dao ;

    @InjectMocks
    private NoteServiceImpl noteService;

    @Test
    public void createNoteTest() {
        Note note = new Note() ;
        note.setNote(15);
        when(dao.save(ArgumentMatchers.any(Note.class))).thenReturn(note) ;

        NoteDTO created = noteService.createNote(NoteConvert.getInstance().convertEntityToDto(note));
        assertEquals(created.getNote(), note.getNote()) ;
        verify(dao).save(note) ;
    }

    @Test
    public void getAllNoteTest() {
        ArrayList<Note> note_list = new ArrayList<>();
        note_list.add(new Note()) ;
        when(dao.findAll()).thenReturn(note_list) ;

        List<NoteDTO> expected = noteService.listNotes();
        List<Note> ex = NoteConvert.getInstance().convertDtoListToEntityList(new ArrayList<>(expected));
        assertEquals(ex, note_list);
        verify(dao).findAll() ;
    }

    @Test
    public void getOneNoteTest() {
        Note note = new Note() ;
        note.setNote(15);

        when(dao.findById(note.getId())).thenReturn(Optional.of(note));

        NoteDTO res = noteService.findNoteById(note.getId()) ;

        assertEquals(res.getNote(), note.getNote());
    }

    @Test
    public void deleteNoteByIdTest() {
        Note note = new Note() ;
        note.setId(42);

        noteService.deleteNoteById(note.getId());
        verify(dao).deleteById(note.getId());
    }

    @Test
    public void updateNoteTest() {
        Note note = new Note() ;
        note.setId(13);
        note.setNote(15);
        noteService.createNote(NoteConvert.getInstance().convertEntityToDto(note)) ;

        Note updated_note = new Note() ;
        updated_note.setId(13);
        updated_note.setNote(15);

        when(dao.getReferenceById(note.getId())).thenReturn(note) ;
        when(dao.findById(note.getId())).thenReturn(Optional.of(note)) ;

            // verify update
        noteService.updateNote(
                NoteConvert.getInstance().convertEntityToDto(updated_note)
        );
        //verify(dao).save(updated_note) ;
        verify(dao).getReferenceById(note.getId()) ;

            // assert new value
        NoteDTO res = noteService.findNoteById(note.getId()) ;
        assertEquals(updated_note.getNote(), res.getNote());
    }
}
