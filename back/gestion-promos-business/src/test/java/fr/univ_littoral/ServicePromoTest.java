package fr.univ_littoral;

import fr.univ_littoral.convert.PromoConvert;
import fr.univ_littoral.dao.matiere.IDaoMatiere;
import fr.univ_littoral.dao.promotion.IDaoPromotion;
import fr.univ_littoral.data.Promotion;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.dto.PromoDTO;
import fr.univ_littoral.service.matiere.MatiereServiceImpl;
import fr.univ_littoral.service.promotion.PromoServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ServicePromoTest {

    @Mock
    private IDaoPromotion dao ;

    @InjectMocks
    private PromoServiceImpl promoService;

    @Test
    public void createMatiereTest() {
        Promotion promotion = new Promotion() ;
        promotion.setIntitule("promoTest");
        when(dao.save(ArgumentMatchers.any(Promotion.class))).thenReturn(promotion) ;

        PromoDTO created = promoService.createPromo(PromoConvert.getInstance().convertEntityToDto(promotion));
        assertEquals(created.getIntitule(), promotion.getIntitule()) ;
        verify(dao).save(promotion) ;
    }

    @Test
    public void getAllPromotionTest() {
        ArrayList<Promotion> promotions_list = new ArrayList<>();
        promotions_list.add(new Promotion()) ;
        when(dao.findAll()).thenReturn(promotions_list) ;

        List<PromoDTO> expected = promoService.listPromos();
        List<Promotion> ex = PromoConvert.getInstance().convertDtoListToEntityList(new ArrayList<>(expected));
        assertEquals(ex, promotions_list);
        verify(dao).findAll() ;
    }

    @Test
    public void getOnePromotionTest() {
        Promotion promotion = new Promotion() ;
        promotion.setIntitule("promotionTest");

        when(dao.findById(promotion.getId())).thenReturn(Optional.of(promotion));

        PromoDTO res = promoService.findPromoById(promotion.getId()) ;

        assertEquals(res.getIntitule(), promotion.getIntitule());
    }

    @Test
    public void deletePromotionByIdTest() {
        Promotion promotion = new Promotion() ;
        promotion.setId(42);

        promoService.deletePromoById(promotion.getId());
        verify(dao).deleteById(promotion.getId());
    }

    @Test
    public void updatePromotionTest() {
        Promotion promotion = new Promotion() ;
        promotion.setId(13);
        promotion.setIntitule("promotionTest");
        promoService.createPromo(PromoConvert.getInstance().convertEntityToDto(promotion)) ;

        Promotion updated_promotion = new Promotion() ;
        updated_promotion.setIntitule("new promotionTest");
        updated_promotion.setId(13);

        when(dao.getReferenceById(promotion.getId())).thenReturn(promotion) ;
        when(dao.findById(promotion.getId())).thenReturn(Optional.of(promotion)) ;

            // verify update
        promoService.updatePromo(
                PromoConvert.getInstance().convertEntityToDto(updated_promotion)
        );
        verify(dao).save(updated_promotion) ;
        verify(dao).getReferenceById(promotion.getId()) ;

            // assert new value
        PromoDTO res = promoService.findPromoById(promotion.getId()) ;
        assertEquals(updated_promotion.getIntitule(), res.getIntitule());
    }
}
