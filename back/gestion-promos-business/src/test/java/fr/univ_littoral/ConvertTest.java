package fr.univ_littoral;

import fr.univ_littoral.convert.EtudiantConvert;
import fr.univ_littoral.convert.MatiereConvert;
import fr.univ_littoral.convert.NoteConvert;
import fr.univ_littoral.convert.PromoConvert;
import fr.univ_littoral.data.Etudiant;
import fr.univ_littoral.data.Matiere;
import fr.univ_littoral.data.Note;
import fr.univ_littoral.data.Promotion;
import fr.univ_littoral.dto.EtudiantDTO;
import fr.univ_littoral.dto.MatiereDTO;
import fr.univ_littoral.dto.NoteDTO;
import fr.univ_littoral.dto.PromoDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;


import java.sql.Date;


public class ConvertTest {
    private static Etudiant etu_do ;
    private static EtudiantDTO etu_dto ;
    private static Matiere matiere_do ;
    private static MatiereDTO matiere_dto ;
    private static Promotion promotion_do ;
    private static PromoDTO promotion_dto ;
    private static Note note_do ;
    private static NoteDTO note_dto ;
    private static Date date ;

    @BeforeAll
    static void init() {
        date = new Date(2001, 3, 7) ;
        etu_do = new Etudiant(1, "nameTest", "FnameTest", 1, "ine123", date, 123) ;
        matiere_do = new Matiere(1, "matiereTest") ;
        promotion_do = new Promotion("promoTest", 2024) ;
        note_do = new Note(15, 1, 1) ;

        etu_dto = new EtudiantDTO() ;
        etu_dto.setNom("nameTest");
        etu_dto.setPrenom("FnameTest");
        etu_dto.setDate_naissance(date);

        matiere_dto = new MatiereDTO() ;
        matiere_dto.setIntitule("matiereTest");
        matiere_dto.setId_promotion(1);

        promotion_dto = new PromoDTO() ;
        promotion_dto.setIntitule("promoTest");
        promotion_dto.setAnnee(2024);

        note_dto = new NoteDTO() ;
        note_dto.setId_matiere(1);
        note_dto.setId_etudiant(1);
        note_dto.setNote(15);
    }

    @Test
    public void EtudiantDoToDtoTest() {
        EtudiantDTO converted = EtudiantConvert.getInstance().convertEntityToDto(etu_do);
        assertEquals(etu_dto.getPrenom(), converted.getPrenom()) ;
        assertEquals(etu_dto.getNom(), converted.getNom());
        assertEquals(etu_dto.getDate_naissance(), converted.getDate_naissance());
    }

    @Test
    public void EtudiantDtoToDoTest() {
        Etudiant converted = EtudiantConvert.getInstance().convertDtoToEntity(etu_dto);
        assertEquals(etu_do.getNom(), converted.getNom());
        assertEquals(etu_do.getPrenom(), converted.getPrenom());
        assertEquals(etu_do.getDateNaissance(), converted.getDateNaissance());
    }

    @Test
    public void MatiereDoToDtoTest() {
        MatiereDTO converted = MatiereConvert.getInstance().convertEntityToDto(matiere_do);
        assertEquals(matiere_dto.getIntitule(), converted.getIntitule());
        assertEquals(matiere_dto.getId_promotion(), converted.getId_promotion());
    }

    @Test
    public void MatiereDtoToDoTest() {
        Matiere converted = MatiereConvert.getInstance().convertDtoToEntity(matiere_dto);
        assertEquals(matiere_do.getIntitule(), converted.getIntitule());
        assertEquals(matiere_do.getIdPromotion(), converted.getIdPromotion());
    }

    @Test
    public void PromoDoToDtoTest() {
        PromoDTO converted = PromoConvert.getInstance().convertEntityToDto(promotion_do);
        assertEquals(promotion_dto.getAnnee(), converted.getAnnee());
        assertEquals(promotion_dto.getIntitule(), converted.getIntitule());
    }

    @Test
    public void PromoDtoToDoTest() {
        Promotion converted = PromoConvert.getInstance().convertDtoToEntity(promotion_dto);
        assertEquals(promotion_do.getAnnee(), converted.getAnnee());
        assertEquals(promotion_do.getIntitule(), converted.getIntitule());
    }

    @Test
    public void NoteDoToDtoTest() {
        NoteDTO converted = NoteConvert.getInstance().convertEntityToDto(note_do);
        assertEquals(note_dto.getNote(), converted.getNote());
        assertEquals(note_dto.getId_etudiant(), converted.getId_etudiant());
        assertEquals(note_dto.getId_matiere(), converted.getId_matiere());
    }

    @Test
    public void NoteDtoToDoTest() {
        Note converted = NoteConvert.getInstance().convertDtoToEntity(note_dto);
        assertEquals(note_do.getNote(), converted.getNote());
        assertEquals(note_do.getIdEtudiant(), converted.getIdEtudiant());
        assertEquals(note_do.getIdMatiere(), converted.getIdMatiere());
    }

}
