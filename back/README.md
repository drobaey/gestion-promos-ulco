## Informations sur la structure du backend / de l'API

Le backend est réalisé en Java SpringBoot. Il est divisé en 3 parties : la couche de présentation, la couche 
business et la couche persistance (c'est la même organisation que celle vue en cours). 

Les tests unitaires sont égalements disponibles (pour la partie business et présentation).

La base de données qu'on utilise est commune à l'ensemble de notre promotion. 

L'API est également disponible en ligne à l'adresse suivante : https://oracle.anthony-jeanney.fr/gestionPromos/api/ .  
Il faut cependant indiquer une route, sinon la page affichera une erreur.
Par exemple, https://oracle.anthony-jeanney.fr/gestionPromos/api/etudiant permet de récupérer la liste des étudiants.


## Lancement du backend / de l'API
Pour lancer le backend, il suffit d'exécuter le fichier Api.java de la couche de présentation (via IntelliJ par exemple).  
Nous avons également ajouté le fichier .jar généré dans le dossier back/, qui permet directement de lancer l'API.  
Pour l'exécuter (depuis le dossier back/):  
```
java -jar gestion-promos-presentation-0.0.1-SNAPSHOT.jar
```