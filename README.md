# Gestion des promotions de l'ULCO

Projet réalisé dans le cadre du module "JEE" en M2.  

Notre partie consistait à gérer les promotions de l'ULCO. Nous n'avons pas eu besoin d'utiliser l'API des autres groupes. Par contre, certains groupes avaient
besoin de notre API. Pour cela, un membre de notre promotion de M2 (Anthony) a mis en ligne une API GetWay, où il est possible d'accéder à notre API (voir README du dossier back/). 
C'est également pour cela que nous avons inclus de la CI à notre projet, car elle génère l'image Docker pour lancer notre API.

Le backend est developpé en Java SpringBoot.  
Le frontend est developpé en ReactJS.  
Pour lancer le projet ou pour avoir plus d'information sur la structure du projet (front / back), se référer aux README dans les sous-dossiers respectifs.

Membres du groupe : 
- ROBAEY Dennis
- VANDEWALLE Charles

Lien trello : https://trello.com/b/rKI889OI/gestion-promos-ulco  
Les images du MCD et du schéma de la BDD sont également disponibles à la racine du projet.
