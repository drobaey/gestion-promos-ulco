import { Routes, Route, Navigate } from 'react-router-dom'
import Home from "./pages/Home/Home.tsx";
import ManagePromotions from "./pages/ManagePromotions/ManagePromotions.tsx";
import ManageSubjects from "./pages/ManageSubjects/ManageSubjects.tsx";
import ManageGrades from "./pages/ManageGrades/ManageGrades.tsx";
import ManageStudents from "./pages/ManageStudents/ManageStudents.tsx";

function App() {
  return (
    <>
        <Routes>
            <Route path="/home" element={
                <Home />
            } />

            <Route path="/manage-promotions" element={
                <ManagePromotions />
            } />

            <Route path="/manage-subjects" element={
                <ManageSubjects />
            } />

            <Route path="/manage-students" element={
                <ManageStudents />
            } />

            <Route path="/manage-grades" element={
                <ManageGrades />
            } />

            <Route path="*" element={
                <Navigate replace to="/home"/>
            } />
        </Routes>
    </>
  )
}

export default App;
