import axios from "axios";
import {useEffect, useState} from "react";
import {
    AdditionalColumnsDictType,
    AdditionalColumnsPossibleValuesType,
    OriginalColumnsArrayType, PossibleValuesType, RowType
} from "../types/TableManagement/TableManagementTypes.ts";

interface UseTableRowManagementProps {
    readonly originalColumnsArray: OriginalColumnsArrayType;
    readonly additionalColumnsDict: AdditionalColumnsDictType;
    // L'URL de l'API à appeler pour récupérer les données de la table originelle
    readonly apiUrl: string;
}

/**
 * Hook permettant de gérer les lignes d'une table (ajout, suppression, édition).
 */
function useTableRowManagement({originalColumnsArray, additionalColumnsDict, apiUrl} : UseTableRowManagementProps) {
    const [rows, setRows] = useState<RowType[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [additionalColumnsPossibleValues, setAdditionalColumnsPossibleValues] =
        useState<AdditionalColumnsPossibleValuesType>({});

    useEffect(() => {
        /**
         * Récupère les données de la table originelle et des tables référencées. Récupère également l'ensemble des
         * valeurs possibles pour les colonnes supplémentaires.
         * @return {Promise<void>}
         */
        const fetchAdditionalData = async (): Promise<void> => {
            try {
                // pour chaque colonne supplémentaire, on récupère l'ensemble des données de la table référencée
                const additionalColumnsPossibleValues: AdditionalColumnsPossibleValuesType = {};

                for (const [additionalColumnName, additionalColumnInfo] of Object.entries(additionalColumnsDict)) {
                    const response = await axios.get(additionalColumnInfo["apiUrl"]);

                    // dictionnaire contenant les valeurs possibles de la colonne supplémentaire
                    // la clé est l'id de la ligne dans la table référencée, la valeur est la valeur de la colonne
                    const possibleValues: PossibleValuesType = {};

                    for (let i = 0; i < response.data.length; i++) {
                        let value = "";

                        // On récupère les colonnes qu'on souhaite récupérer et afficher de la table référencée. On sépare
                        // les valeurs par un espace. Ex : "name year" pour une promotion
                        for (const columnName of additionalColumnInfo["columnsToFetch"]) {
                            value += response.data[i][columnName];
                            value += " ";
                        }

                        value = value.trim();

                        possibleValues[response.data[i]["id"]] = value;
                    }

                    additionalColumnsPossibleValues[additionalColumnName] = possibleValues;
                }

                setAdditionalColumnsPossibleValues(additionalColumnsPossibleValues);

                const response = await axios.get(apiUrl);

                const rows: RowType[] = []

                // pour chaque ligne de la table originelle, on récupère les colonnes de base, ainsi que
                // les colonnes supplémentaires provenant d'une autre table (ex : promotionNameAndYear pour une matière)
                for (let i = 0; i < response.data.length; i++) {
                    rows.push({})

                    // on récupère les valeurs des colonnes de base
                    for (const columnName of originalColumnsArray) {
                        rows[i][columnName] = response.data[i][columnName]
                    }

                    // on récupère les valeurs des colonnes supplémentaires
                    for (const [additionalColumnName, additionalColumnInfo] of Object.entries(additionalColumnsDict)) {
                        // la clé étrangère référencée dans la table originelle (ex : id_promotion dans la table matière)
                        const id_of_foreign_table: number = rows[i][additionalColumnInfo["id"]] as number;

                        // on récupère la valeur de la colonne supplémentaire pour la ligne actuelle
                        rows[i][additionalColumnName] = additionalColumnsPossibleValues[additionalColumnName][
                            id_of_foreign_table
                        ];
                    }
                }

                setRows(rows);
            } catch (error) {
                console.log(error);
            }
        }


        fetchAdditionalData()
            .then(() => {
                setIsLoading(false);
            }).catch(error => {
                console.log(error);
            })
    }, [additionalColumnsDict, apiUrl, originalColumnsArray]);


    /**
     * Gère la suppression d'une ligne. Si la suppression est impossible (car la ligne est référencée dans une autre
     * table), affiche une alerte.
     *
     * @param {number} id  L'id de la ligne à supprimer
     * @return {Promise<void>}
     */
    const handleDeleteRow = async (id: number): Promise<void> => {
        try {
            await axios.delete(apiUrl + "/" + id);

            // mise à jour des lignes
            setRows(
                rows.filter((row) => {
                    return row.id !== id;
                })
            );
        } catch (error: unknown) {
            console.error(error);
            if (axios.isAxiosError(error) && error.response?.status === 409) {
                alert("Impossible de supprimer cet élément car il est utilisé dans une autre table");
            }
        }
    }

    /**
     * Gère l'ajout d'une ligne.
     *
     * @param {Object} rowToAdd  La ligne à ajouter
     * @return {Promise<void>}
     */
    const handleAddRow = async (rowToAdd: RowType): Promise<void> => {
        try {
            const response = await axios.post(apiUrl, rowToAdd);

            const newRow: RowType = {};

            // on récupère les valeurs des colonnes de base
            for (const columnName of originalColumnsArray) {
                newRow[columnName] = response.data[columnName]
            }

            // on récupère les valeurs des colonnes supplémentaires (ex : promotionNameAndYear pour une matière)
            for (const [additionalColumnName, additionalColumnInfo] of Object.entries(additionalColumnsDict)) {
                const id_of_foreign_table = rowToAdd[additionalColumnInfo["id"]] as number;
                newRow[additionalColumnName] = additionalColumnsPossibleValues[additionalColumnName][
                    id_of_foreign_table
                ];
            }

            setRows([...rows, newRow]);
        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Gère l'édition d'une ligne.
     *
     * @param {Object} rowToEdit  La ligne à éditer
     * @return {Promise<void>}
     */
    const handleEditRow = async (rowToEdit: RowType): Promise<void> => {
        console.log(rowToEdit) // TODO NewRowType -> OriginalRowType ? vérifier autres fonctions

        try {
            await axios.put(apiUrl + "/" + rowToEdit.id, rowToEdit);

            setRows(
                rows.map((row) => {
                    // si la ligne à éditer est la ligne actuelle, on met à jour les valeurs
                    if (rowToEdit.id === row.id) {
                        const editedRow: RowType = {};

                        // on récupère les valeurs des colonnes de base
                        for (const columnName of originalColumnsArray) {
                            editedRow[columnName] = rowToEdit[columnName];
                        }

                        // on récupère les valeurs des colonnes supplémentaires
                        // (ex : promotionNameAndYear pour une matière)
                        for (const [additionalColumnKey, additionalColumnInfo] of Object.entries(additionalColumnsDict)) {
                            const id_of_foreign_table = rowToEdit[additionalColumnInfo["id"]] as number;
                            editedRow[additionalColumnKey] = additionalColumnsPossibleValues[additionalColumnKey][
                                id_of_foreign_table
                            ];
                        }

                        // on retourne la ligne éditée
                        return editedRow;
                    }

                    // sinon, on retourne la ligne actuelle
                    return row;
                })
            );
        } catch (error) {
            console.error(error);
        }
    }

    return { rows, isLoading, additionalColumnsPossibleValues, handleDeleteRow, handleAddRow, handleEditRow };
}

export default useTableRowManagement;