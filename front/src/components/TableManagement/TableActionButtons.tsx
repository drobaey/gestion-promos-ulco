import {MdOutlineEdit} from "react-icons/md";
import {RiAddCircleLine} from "react-icons/ri";
import {IoTrashOutline} from "react-icons/io5";
import DropdownFiMoreVertical from "../Dropdown/DropdownFiMoreVertical.tsx";
import {Link} from "react-router-dom";
import {MoreButtonLinksInfoArrayType, RowType} from "../../types/TableManagement/TableManagementTypes.ts";

interface TableActionButtonsProps {
    onAddButtonClick?: () => void;
    onEditButtonClick?: () => void;
    onDeleteButtonClick?: () => void;
    data?: RowType;
    moreButtonLinksInfoArray?: MoreButtonLinksInfoArrayType;
}

/**
 * Composant représentant des boutons d'action pour un tableau. Si une des fonctions n'est pas spécifié (undefined),
 * nous n'affichons pas le bouton correspondant.
 */
function TableActionButtons ({onAddButtonClick, onEditButtonClick, onDeleteButtonClick, data,
                                 moreButtonLinksInfoArray} : TableActionButtonsProps) {

    const linkItems = [];

    if (moreButtonLinksInfoArray && data) {
        for (const linkInfo of moreButtonLinksInfoArray) {
            let searchValue = ""

            for (const columnToGet of linkInfo["columnsToGet"]) {
                searchValue += data[columnToGet];
                searchValue += " ";
            }

            searchValue = searchValue.trim();

            const url = linkInfo["baseUrl"] + "?searchValue=" + searchValue;

            linkItems.push(
                <Link key={url} to={url}>
                    <li className="px-4 py-2 text-sm w-full hover:bg-gray-300 hover:cursor-pointer">
                        {linkInfo["label"]}
                    </li>
                </Link>
            );
        }
    }

    return (
        <div className={"mx-6 flex items-center justify-between py-6 text-2xl"}>
            { onAddButtonClick && (
                <button
                    onClick={onAddButtonClick}
                    className={"flex items-center justify-center h-11 w-11 hover:bg-gray-400 " +
                        "rounded-lg transition-colors duration-200 focus:outline-none"}
                >
                    <RiAddCircleLine />
                </button>
            )}

            { onEditButtonClick && (
                <button
                    onClick={onEditButtonClick}
                    className={"flex items-center justify-center h-11 w-11 hover:bg-gray-400 " +
                        "rounded-lg transition-colors duration-200 focus:outline-none"}
                >
                    <MdOutlineEdit />
                </button>
            )}

            { onDeleteButtonClick && (
                <button
                    onClick={onDeleteButtonClick}
                    className={"flex items-center justify-center h-11 w-11 hover:bg-gray-400 " +
                        "rounded-lg transition-colors duration-200 focus:outline-none"}
                >
                    <IoTrashOutline/>
                </button>
            )}

            { moreButtonLinksInfoArray && (
                <DropdownFiMoreVertical
                    buttonClassName={"flex items-center justify-center h-11 w-11 hover:bg-gray-400 rounded-lg " +
                        "transition-colors duration-200 focus:outline-none"}
                >
                    { linkItems }
                </DropdownFiMoreVertical>
            )}
        </div>
    )
}

export default TableActionButtons;