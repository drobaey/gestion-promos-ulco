import {Link} from "react-router-dom";

function TableManagementFooter () {
    return (
        <div className={"flex flex-col items-center justify-center pb-8"}>
            <div className={"flex items-end justify-center mt-4"}>
                Accéder et gérer les :
            </div>
            <div className={"flex items-center justify-center space-x-4 mt-1"}>

                <Link to={"/manage-students"} className={"underline focus:outline-none"}>
                    Étudiants
                </Link>

                <Link to={"/manage-subjects"} className={"underline focus:outline-none"}>
                    Matières
                </Link>

                <Link to={"/manage-promotions"} className={"underline focus:outline-none"}>
                    Promotions
                </Link>

                <Link to={"/manage-grades"} className={"underline focus:outline-none"}>
                    Notes
                </Link>
            </div>
        </div>
    );
}

export default TableManagementFooter;