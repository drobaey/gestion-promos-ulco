import TableManagement from "./TableManagement.tsx";
import useTableRowManagement from "../../hooks/useTableRowManagement.tsx";
import TableManagementHeader from "./TableManagementHeader.tsx";
import LoadingSpinner from "../LoadingSpinner.tsx";
import TableManagementFooter from "./TableManagementFooter.tsx";
import {
    AdditionalColumnsDictType, MoreButtonLinksInfoArrayType, OriginalColumnsArrayType, RowType,
    ShowedColumnsLabelsDictType, ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

interface TableManagementPageProps {
    readonly originalColumnsArray: OriginalColumnsArrayType;
    readonly additionalColumnsDict: AdditionalColumnsDictType;
    readonly showedColumnsLabelsDict: ShowedColumnsLabelsDictType;
    readonly showedColumnsTypesDict: ShowedColumnsTypesDictType;
    readonly moreButtonLinksInfoArray?: MoreButtonLinksInfoArrayType;
    // L'URL de l'API pour récupérer les données de la table originale
    readonly apiUrl: string;
    // Le nom de la page (pour l'afficher dans le header)
    readonly pageName: string;
}

function TableManagementPage({originalColumnsArray, additionalColumnsDict, showedColumnsLabelsDict,
                                 showedColumnsTypesDict, moreButtonLinksInfoArray = undefined, apiUrl,
                                 pageName} : TableManagementPageProps) {

    const {
        rows,
        isLoading,
        additionalColumnsPossibleValues,
        handleDeleteRow,
        handleAddRow,
        handleEditRow
    } = useTableRowManagement({
        originalColumnsArray: originalColumnsArray,
        additionalColumnsDict: additionalColumnsDict,
        apiUrl: apiUrl
    })

    return (
        <div className={"bg-[#dddddd] w-full h-full flex flex-col justify-between items-center overflow-y-auto"}>
            <div>
                <TableManagementHeader pageName={pageName} />

                { isLoading ?
                    <LoadingSpinner />
                    :
                    <TableManagement
                        data={rows}
                        showedColumnsLabelsDict={showedColumnsLabelsDict}
                        showedColumnsTypesDict={showedColumnsTypesDict}
                        additionalColumnsDict={additionalColumnsDict}
                        additionalColumnsPossibleValues={additionalColumnsPossibleValues}
                        moreButtonLinksInfoArray={moreButtonLinksInfoArray}
                        onAdd={async (newData: RowType) => {await handleAddRow(newData)}}
                        onEdit={async (editedData: RowType) => {await handleEditRow(editedData)}}
                        onDelete={async (id: number) => {await handleDeleteRow(id)}}
                    />
                }
            </div>

            <TableManagementFooter />
        </div>
    );
}

export default TableManagementPage;