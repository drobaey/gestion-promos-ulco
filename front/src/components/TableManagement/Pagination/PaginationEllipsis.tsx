function PaginationEllipsis() {
    return (
        <div
            className={"focus:outline-none h-11 w-11  mx-1 flex items-center justify-center hover:cursor-default"}
        >
            ...
        </div>
    )
}

export default PaginationEllipsis;