import React from "react";

interface PaginationItemProps {
    isActive: boolean;
    onClick: () => void;
    children: React.ReactNode;
}

function PaginationItem({isActive, onClick, children} : PaginationItemProps) {
    return (
        <button
            onClick={onClick}
            className={`h-11 w-11 rounded-lg mx-1 transition-colors duration-200 flex items-center justify-center 
            ${isActive ? 'text-white bg-blue-600' : 'hover:bg-gray-400'} focus:outline-none`}
            disabled={isActive}
        >
            {children}
        </button>
    )


}

export default PaginationItem;