import PaginationItem from "./PaginationItem.tsx";
import {useSearchParams} from "react-router-dom";
import PaginationEllipsis from "./PaginationEllipsis.tsx";
import PaginationChevron from "./PaginationChevron.tsx";
import React from "react";

interface PaginationProps {
    // Le nombre total de pages
    totalPages: number;
    // L'index de la page actuelle
    pageIndex: number;
}

function Pagination({totalPages, pageIndex} : PaginationProps) {
    const [, setSearchParams] = useSearchParams();

    const paginationItems: Array<React.ReactElement> = [];

    for (let i = 1; i <= totalPages; i++) {
        paginationItems.push(
            <PaginationItem
                key={i}
                isActive={i === pageIndex}
                onClick={() => setSearchParams({pageIndex: String(i)})}
            >
                {i}
            </PaginationItem>
        );
    }

    function renderMiddlePagination() {
        if (totalPages <= 5) {
            return (
                <>
                    {paginationItems}
                </>
            )
        }

        if (pageIndex <= 3) {
            return (
                <>
                    {paginationItems.slice(0, 3)}
                    <PaginationEllipsis />
                    {paginationItems[totalPages - 1]}
                </>
            )
        }


        if (pageIndex > 3 && pageIndex < totalPages - 2) {
            return (
                <>
                    {paginationItems[0]}
                    <PaginationEllipsis />
                    {paginationItems[pageIndex-1]}
                    <PaginationEllipsis />
                    {paginationItems[totalPages - 1]}
                </>
            )
        }

        // pageIndex >= totalPages - 2
        return (
            <>
                {paginationItems[0]}
                <PaginationEllipsis />
                {paginationItems.slice(totalPages-3, totalPages)}
            </>
        )
    }

    return (
        <div className={"flex flex-row items-center justify-center text-xl"}>
            <PaginationChevron
                direction={"left"}
                onClick={() => {
                    setSearchParams({
                        pageIndex: pageIndex - 1 > 0 ?
                            String(pageIndex - 1) : String(1)
                    })
                }}
                disabled={pageIndex === 1}
            />

            {renderMiddlePagination()}

            <PaginationChevron
                direction={"right"}
                onClick={() => {
                    setSearchParams({
                        pageIndex: pageIndex + 1 < totalPages ?
                            String(pageIndex + 1) : String(totalPages)
                    })
                }}
                disabled={pageIndex === totalPages}
            />
        </div>
    );
}

export default Pagination;