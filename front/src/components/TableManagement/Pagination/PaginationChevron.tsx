import {FaChevronLeft, FaChevronRight} from "react-icons/fa";

interface PaginationChevronProps {
    direction: "left" | "right";
    onClick: () => void;
    disabled: boolean;
}

function PaginationChevron({direction, onClick, disabled} : PaginationChevronProps) {
    return (
        <button
            onClick={onClick}
            className={`focus:outline-none h-11 w-11 rounded-lg mx-1 transition-colors duration-200 flex
             justify-center items-center ${disabled ? 'hover:bg-none text-gray-400' : 'hover:bg-gray-400'} `}
            disabled={disabled}
        >
            {direction === "left" ? (
                <FaChevronLeft className={"text-sm"}/>
            ) : (
                <FaChevronRight className={"text-sm"}/>
            )}
        </button>
    )
}

export default PaginationChevron;