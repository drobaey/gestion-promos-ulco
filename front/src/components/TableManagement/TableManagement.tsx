import {useSearchParams} from "react-router-dom";
import SearchInput from "../Inputs/SearchInput.tsx";
import Pagination from "./Pagination/Pagination.tsx";
import Table from "./Table.tsx";
import {useEffect, useState} from "react";
import TableActionButtons from "./TableActionButtons.tsx";
import {
    AdditionalColumnsDictType, AdditionalColumnsPossibleValuesType, MoreButtonLinksInfoArrayType, RowType,
    ShowedColumnsLabelsDictType,
    ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

interface TableManagementProps {
    data: Array<RowType>;
    readonly showedColumnsLabelsDict: ShowedColumnsLabelsDictType;
    readonly showedColumnsTypesDict: ShowedColumnsTypesDictType;
    readonly additionalColumnsDict: AdditionalColumnsDictType;
    readonly additionalColumnsPossibleValues: AdditionalColumnsPossibleValuesType;
    readonly moreButtonLinksInfoArray?: MoreButtonLinksInfoArrayType;
    onAdd: (newData: RowType) => Promise<void>;
    onEdit: (editedData: RowType) => Promise<void>;
    onDelete: (id: number) => Promise<void>;
}

function TableManagement({data, showedColumnsLabelsDict, showedColumnsTypesDict, additionalColumnsDict,
                             additionalColumnsPossibleValues, moreButtonLinksInfoArray, onAdd, onEdit,
                             onDelete} : TableManagementProps) {
    const [searchParams, setSearchParams] = useSearchParams();
    const [isCreating, setIsCreating] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [searchValue, setSearchValue] = useState("");
    // l'élément qui est en train d'être créé (sans id)
    const [newData, setNewData] = useState<RowType>({});
    // l'élément qui est en train d'être édité (avec id)
    const [editedData, setEditedData] = useState<RowType>({})

    useEffect(() => {
        const localSearchValue = searchParams.get("searchValue");
        if (localSearchValue !== null) {
            setSearchValue(localSearchValue);
        }
    }, [searchParams]);

    const filteredData: RowType[] = data.filter((row: RowType) => {
        if (searchValue === "") {
            return row;
        } else {
            // on retourne les éléments qui, séparés par un espace, contiennent tous les mots de la recherche
            return searchValue.trim().split(" ").every((word) => {
                return Object.values(row).join(" ").toLowerCase().includes(word.toLowerCase());
            });
        }
    });

    let pageIndex: number;
    const pageIndexFromUrl = searchParams.get("pageIndex");

    if (pageIndexFromUrl === null) {
        pageIndex =  1;
    } else {
        pageIndex = parseInt(pageIndexFromUrl);
    }

    if (pageIndex > filteredData.length) {
        pageIndex = filteredData.length;
    }

    if (pageIndex < 1 || filteredData.length === 0) {
        pageIndex = 1;
    }

    /**
     * Met à jour les données de l'élément en cours de création
     * @param columnName
     * @param value
     */
    const handleSetNewData = (columnName: string, value: string | number) => {
        const localNewData = {
            ...newData
        }

        if (columnName in additionalColumnsDict) {
            // on assigne l'id de la table référencée dans la table originale
            // ex : id_promotion pour la table matiere
            const foreignKeyColumnName = additionalColumnsDict[columnName]["id"];
            localNewData[foreignKeyColumnName] = value; // ex : localNewData["id_promotion"] = 1
        } else {
            // si la colonne est dans la table originale (ex : intitule pour la table matiere)
            localNewData[columnName] = value;
        }

        setNewData(localNewData);
    }

    /**
     * Met à jour les données de l'élément en cours d'édition
     * @param columnName
     * @param value
     */
    const handleSetEditData = (columnName: string, value: string | number) => {
        const localEditedData = {
            ...editedData,
            [columnName]: value
        };

        // si la colonne est une colonne supplémentaire non présente dans la table originale
        // ex : promotionNameAndYear pour la table matiere
        // dans ce cas, value est l'id de la table référencée, et forcément un nombre
        if (columnName in additionalColumnsDict) {
            // on assigne l'id de la table référencée dans la table originale
            // ex : id_promotion pour la table matiere
            const foreignKeyColumnName = additionalColumnsDict[columnName]["id"];
            localEditedData[foreignKeyColumnName] = value; // ex : localEditedData["id_promotion"] = 1

            // on assigne la valeur de la colonne supplémentaire pour l'affichage
            // ex pour la table matière : localEditedData["promotionNameAndYear"] = "M2 WEBSCI 2023"
            localEditedData[columnName] = additionalColumnsPossibleValues[columnName][value as number];
        }

        setEditedData(localEditedData);
    }

    const openAddMenu = () => {
        setIsCreating(true);
        setNewData( {});
    }

    const openEditMenu = () => {
        setIsEditing(true);
        setEditedData(JSON.parse(JSON.stringify(filteredData[pageIndex - 1]))); // deep copy
    }

    /**
     * Supprime l'élément actuellement affiché
     */
    const deleteData = async () => {
        await onDelete(filteredData[pageIndex-1]["id"] as number);
    }

    /**
     * Crée un nouvel élément
     */
    const createData = async (newData: RowType) => {
        const newDataValues = Object.values(newData);

        // on vérifie que toutes les valeurs sont renseignées
        if (newDataValues.length !== Object.values(showedColumnsTypesDict).length
            || newDataValues.some(value => !value)) {
            window.alert("Veuillez remplir tous les champs.");
            return;
        }

        await onAdd(newData);
        setSearchValue("");
        // mise à jour de l'index pour afficher l'élément créé
        setSearchParams({pageIndex: String(data.length + 1)});
        setIsCreating(false);
    }

    /**
     * Modifie l'élément actuellement affiché
     */
    const editData = async (editedData: RowType) => {
        const editedDataValues = Object.values(editedData);

        // On vérifie que toutes les valeurs sont renseignées
        if (editedDataValues.some(value => !value)) {
            window.alert("Veuillez remplir tous les champs.");
            return;
        }

        await onEdit(editedData);
        setSearchValue("");
        setIsEditing(false);
    }

    if (isCreating || isEditing) {
        return (
            <>
                <Table
                    data={isCreating ? newData : filteredData[pageIndex - 1]}
                    isCreating={isCreating}
                    isEditing={isEditing}
                    showedColumnsLabelsDict={showedColumnsLabelsDict}
                    showedColumnsTypesDict={showedColumnsTypesDict}
                    additionalColumnsDict={additionalColumnsDict}
                    additionalColumnsPossibleValues={additionalColumnsPossibleValues}
                    setData={isCreating ? handleSetNewData : handleSetEditData}
                />

                <div className={"flex items-center justify-center py-8 space-x-16 text-xl ml-5"}>
                    <button
                        className={"bg-gray-500 hover:bg-gray-600 text-white px-4 py-2 rounded-lg"}
                        onClick={() => {
                            if (isCreating) {
                                setIsCreating(false)
                                setNewData({})
                            } else { // isEditing
                                setIsEditing(false);
                                setEditedData(filteredData[pageIndex - 1])
                            }
                        }}
                    >
                        Annuler
                    </button>
                    <button
                        className={"bg-blue-600 hover:bg-blue-700 text-white px-4 py-2 rounded-lg text-xl"}
                        onClick={ async () => {
                            if (isCreating) {
                                await createData(newData)
                            } else {
                                await editData(editedData)
                            }
                        }}
                    >
                        Confirmer
                    </button>
                </div>
            </>
        );
    }

    return (
        <>
            <div className={"w-full px-2 flex flex-row mb-4 space-x-2"}>
                <div className={"w-full"}>
                    <SearchInput
                        handleInputChange={(e) => {
                            setSearchValue(e.target.value);
                        }}
                        value={searchValue}
                    />
                </div>
            </div>

            {filteredData.length === 0 ? (
                <>
                    <div className={"flex items-center justify-center mt-10"}>
                        Aucun élément trouvé.
                    </div>
                    <div className={"flex flex-row items-center justify-center"}>
                        Ajouter un élément :
                        <TableActionButtons
                            onAddButtonClick={openAddMenu}
                        />
                    </div>
                </>
            ) : (
                <>
                    <Table
                        data={filteredData[pageIndex - 1]}
                        showedColumnsLabelsDict={showedColumnsLabelsDict}
                        showedColumnsTypesDict={showedColumnsTypesDict}
                        additionalColumnsDict={additionalColumnsDict}
                        additionalColumnsPossibleValues={additionalColumnsPossibleValues}
                    />

                    <TableActionButtons
                        onAddButtonClick={openAddMenu}
                        onEditButtonClick={openEditMenu}
                        onDeleteButtonClick={deleteData}
                        data={filteredData[pageIndex - 1]}
                        moreButtonLinksInfoArray={moreButtonLinksInfoArray}
                    />
                </>
            )}

            <Pagination
                totalPages={filteredData.length > 1 ? filteredData.length : 1}
                pageIndex={pageIndex}
            />
        </>
    );
}

export default TableManagement;