import EditOrCreateTableEntry from "./EditOrCreateTableEntry.tsx";
import {
    AdditionalColumnsDictType, AdditionalColumnsPossibleValuesType, RowType,
    ShowedColumnsLabelsDictType,
    ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

interface TableProps {
    data: RowType;
    isCreating?: boolean;
    isEditing?: boolean;
    showedColumnsLabelsDict: ShowedColumnsLabelsDictType;
    showedColumnsTypesDict: ShowedColumnsTypesDictType;
    additionalColumnsDict: AdditionalColumnsDictType;
    additionalColumnsPossibleValues: AdditionalColumnsPossibleValuesType;
    setData?: (columnName: string, value: string | number) => void;
}

function Table({data, isCreating=false, isEditing=false, showedColumnsLabelsDict, showedColumnsTypesDict,
                   additionalColumnsDict, additionalColumnsPossibleValues, setData} : TableProps) {

    const rows = Object.keys(showedColumnsLabelsDict).map((columnName) => {
        const columnLabel = showedColumnsLabelsDict[columnName];
        const columnValue = data[columnName];

        return (
            <tr key={columnName}>
                <th className="text-left px-4 py-3.5 border border-gray-400 w-[50%] ">{columnLabel}</th>
                <td className="text-left px-4 py-1.5 border border-gray-400">
                    { isCreating || isEditing ? (
                        <EditOrCreateTableEntry
                            isEditing={isEditing}
                            isCreating={isCreating}
                            columnName={columnName}
                            columnLabel={columnLabel}
                            columnValue={ showedColumnsTypesDict[columnName] === "array" ?
                                data[additionalColumnsDict[columnName]["id"]] : columnValue
                            }
                            showedColumnsTypesDict={showedColumnsTypesDict}
                            additionalColumnsPossibleValues={additionalColumnsPossibleValues}
                            setData={setData!}
                        />
                    ) : (
                        columnValue
                    )}
                </td>
            </tr>
        );
    });

    return (
        <table className="border border-gray-400 w-full">
            <tbody>
                {rows}
            </tbody>
        </table>
    );
}

export default Table;