import {Link} from "react-router-dom";
import logoUlco from "../../assets/images/logoUlco.png";

interface TableManagementHeaderProps {
    pageName: string;
}

function TableManagementHeader({pageName} : TableManagementHeaderProps) {
    return (
        <div className={"flex flex-col items-center justify-center w-full pt-5 mb-8"}>
            <Link to={"/"}>
                <img src={logoUlco} className={"w-[300px] mb-8"}/>
            </Link>
            <div className={"font-bold"}>
                Gestion des {pageName}
            </div>
        </div>
    )
}

export default TableManagementHeader;