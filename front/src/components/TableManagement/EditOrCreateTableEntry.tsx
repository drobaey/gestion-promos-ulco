import {
    AdditionalColumnsPossibleValuesType,
    ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

interface EditOrCreateTableEntryProps {
    isEditing: boolean;
    isCreating: boolean;
    columnName: string;
    columnLabel: string;
    columnValue: string | number;
    showedColumnsTypesDict: ShowedColumnsTypesDictType;
    additionalColumnsPossibleValues: AdditionalColumnsPossibleValuesType;
    setData: (columnName: string, value: string | number) => void;
}

function EditOrCreateTableEntry({isEditing, isCreating, columnName, columnLabel, columnValue, showedColumnsTypesDict,
                        additionalColumnsPossibleValues, setData} : EditOrCreateTableEntryProps) {

    if (showedColumnsTypesDict[columnName] === "text") {
        return (
            <input
                className={"bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg " +
                    "focus:ring-blue-500 focus:border-blue-500 w-full p-2 focus:outline-none"}
                placeholder={columnLabel}
                defaultValue={isEditing ? columnValue : ""}
                onChange={(e) => { setData(columnName, e.target.value)}}
            />
        );
    }

    if (showedColumnsTypesDict[columnName] === "array") {
        return (
            <select
                className={"bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg " +
                    "focus:ring-blue-500 focus:border-blue-500 w-full p-2 focus:outline-none"}
                onChange={(e) => {
                    // parseInt car il s'agit de l'id de la table référencée
                    setData(columnName, parseInt(e.target.value))
                }}

                // la VALUE du select correspond à l'id de la table référencée
                defaultValue={isEditing ? columnValue : ""}
            >
                {isCreating && (
                    <option disabled value={""}>
                        Sélectionner
                    </option>
                )}

                {Object.entries(additionalColumnsPossibleValues[columnName]).map(([rowId, value]) => {
                    return (
                        <option
                            key={rowId}
                            value={rowId}
                        >
                            {value}
                        </option>
                    )
                })}
            </select>
        )
    }

    if (showedColumnsTypesDict[columnName] === "int") {
        return (
            <input
                className={"bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg " +
                    "focus:ring-blue-500 focus:border-blue-500 w-full p-2 focus:outline-none"}
                type="number"
                step="1"
                placeholder={columnLabel}
                defaultValue={isEditing ? columnValue : ""}
                onChange={(e) => { setData(columnName, parseInt(e.target.value))}}
            />
        );
    }

    if (showedColumnsTypesDict[columnName] === "float") {
        return (
            <input
                className={"bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg " +
                    "focus:ring-blue-500 focus:border-blue-500 w-full p-2 focus:outline-none"}
                type="number"
                step="0.01"
                placeholder={columnLabel}
                defaultValue={isEditing ? columnValue : ""}
                onChange={(e) => { setData(columnName, parseFloat(e.target.value))}}
            />
        );
    }

    if (showedColumnsTypesDict[columnName] === "date") {
        return (
            <input
                className={"bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg " +
                    "focus:ring-blue-500 focus:border-blue-500 w-full p-2 focus:outline-none"}
                type="date"
                placeholder={columnLabel}
                defaultValue={isEditing ? columnValue : ""}
                onChange={(e) => { setData(columnName, e.target.value)}}
            />
        );
    }

}

export default EditOrCreateTableEntry;