import React, {useEffect, useRef, useState} from "react";
import {FiMoreVertical} from "react-icons/fi";

interface DropdownFiMoreVerticalProps {
    buttonClassName: string;
    children: React.ReactNode;
}

function DropdownFiMoreVertical({ buttonClassName, children } : DropdownFiMoreVerticalProps) {
    // champs pour gérer l'affichage ou non du dropdown
    const [isDropdownOpen, setIsDropdownOpen] = useState<boolean>(false);
    const dropdownRef = useRef<HTMLDivElement>(null);

    const toggleDropdown = () => {
        setIsDropdownOpen(!isDropdownOpen);
    };

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            // si le clic n'est pas situé dans le dropdown, on retire l'affichage du dropdown
            if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node)) {
                setIsDropdownOpen(false);
            }
        };

        document.addEventListener("click", handleClickOutside);

        // on retire l'eventListener quand on quitte la page
        return () => {
            document.removeEventListener("click", handleClickOutside);
        };
    }, []);

    return (
        <div className="relative" ref={dropdownRef}>
            <button
                className={buttonClassName}
                onClick={toggleDropdown}
            >
                <FiMoreVertical />
            </button>

            { (isDropdownOpen) && (
                <div className={"absolute z-10 mt-2 rounded-md bg-white shadow-lg border right-0 whitespace-nowrap"}>
                    <ul className="py-1" onClick={toggleDropdown}>
                        { children }
                    </ul>
                </div>
            )}
        </div>
    )
}

export default DropdownFiMoreVertical;