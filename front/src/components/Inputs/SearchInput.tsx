import {SlMagnifier} from "react-icons/sl";
import React, {useState} from "react";

interface SearchInputProps {
    handleInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    placeholder?: string;
    value?: string;
}

function SearchInput({handleInputChange, placeholder="Rechercher", value=""}: SearchInputProps) {
    const [isInputFocused, setInputFocused] = useState<boolean>(false);

    const handleInputFocus = () => {
        setInputFocused(true);
    };

    const handleInputBlur = () => {
        setInputFocused(false);
    };

    const divClasses = `flex items-center border rounded-lg transition-shadow p-2 bg-gray-50 ${isInputFocused ? 
        'border-blue-500 ring-blue-500' : 'border-gray-300'}`;

    return (
        <div className={divClasses}>
            <div className="ml-1 mr-3">
                <SlMagnifier />
            </div>
            <input
                placeholder={placeholder}
                className="outline-none w-full p-0 border-none focus:ring-0 bg-gray-50"
                onChange={handleInputChange}
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                value={value}
            />
        </div>
    )
}

export default SearchInput;