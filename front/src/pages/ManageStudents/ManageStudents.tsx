import TableManagementPage from "../../components/TableManagement/TableManagementPage.tsx";
import {
    AdditionalColumnsDictType, MoreButtonLinksInfoArrayType, OriginalColumnsArrayType,
    ShowedColumnsLabelsDictType, ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

function ManageStudents() {
    const originalColumnsArray: OriginalColumnsArrayType = [
        "id",
        "id_promotion",
        "prenom",
        "nom",
        "numero_etudiant",
        "numero_INE",
        "numero_lecteur",
        "date_naissance"
    ] as const;

    const additionalColumnsDict: AdditionalColumnsDictType = {
        promotionNameAndYear: {
            apiUrl: "http://localhost:8080/promotion",
            id: "id_promotion",
            columnsToFetch: ["intitule", "annee"]
        }
    } as const;

    const showedColumnsLabelsDict: ShowedColumnsLabelsDictType = {
        prenom: "Prénom",
        nom: "Nom",
        numero_etudiant: "Numéro étudiant",
        numero_INE: "Numéro INE",
        numero_lecteur: "Numéro lecteur",
        date_naissance: "Date de naissance",
        promotionNameAndYear: "Promotion"
    } as const;

    const showedColumnsTypesDict: ShowedColumnsTypesDictType = {
        prenom: "text",
        nom: "text",
        numero_etudiant: "int",
        numero_INE: "text",
        numero_lecteur: "int",
        date_naissance: "date",
        promotionNameAndYear : "array"
    } as const;

    const moreButtonLinksInfoArray: MoreButtonLinksInfoArrayType = [{
        baseUrl: "/manage-grades",
        columnsToGet: ["nom", "prenom"],
        label: "Voir les notes de l'étudiant"
    }, {
        baseUrl: "/manage-promotions",
        columnsToGet: ["promotionNameAndYear"],
        label: "Voir la promotion"
    }] as const;

    return (
        <TableManagementPage
            originalColumnsArray={originalColumnsArray}
            additionalColumnsDict={additionalColumnsDict}
            showedColumnsLabelsDict={showedColumnsLabelsDict}
            showedColumnsTypesDict={showedColumnsTypesDict}
            moreButtonLinksInfoArray={moreButtonLinksInfoArray}
            apiUrl={"http://localhost:8080/etudiant"}
            pageName={"étudiants"}
        />
    );
}

export default ManageStudents;