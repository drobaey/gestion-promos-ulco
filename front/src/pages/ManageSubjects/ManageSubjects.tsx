import TableManagementPage from "../../components/TableManagement/TableManagementPage.tsx";
import {
    AdditionalColumnsDictType, MoreButtonLinksInfoArrayType, OriginalColumnsArrayType,
    ShowedColumnsLabelsDictType, ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

function ManageSubjects() {
    const originalColumnsArray: OriginalColumnsArrayType = [
        "id",
        "id_promotion",
        "intitule",
    ] as const;

    const additionalColumnsDict: AdditionalColumnsDictType = {
        promotionNameAndYear: {
            apiUrl: "http://localhost:8080/promotion",
            id: "id_promotion",
            columnsToFetch: ["intitule", "annee"]
        }
    } as const;

    const showedColumnsLabelsDict: ShowedColumnsLabelsDictType = {
        intitule: "Intitulé",
        promotionNameAndYear: "Promotion"
    } as const;

    const showedColumnsTypesDict: ShowedColumnsTypesDictType = {
        intitule: "text",
        promotionNameAndYear: "array"
    } as const;

    const moreButtonLinksInfoArray: MoreButtonLinksInfoArrayType = [{
        baseUrl: "/manage-promotions",
        columnsToGet: ["promotionNameAndYear"],
        label: "Voir la promotion"
    }] as const;

    return (
        <TableManagementPage
            originalColumnsArray={originalColumnsArray}
            additionalColumnsDict={additionalColumnsDict}
            showedColumnsLabelsDict={showedColumnsLabelsDict}
            showedColumnsTypesDict={showedColumnsTypesDict}
            moreButtonLinksInfoArray={moreButtonLinksInfoArray}
            apiUrl={"http://localhost:8080/matiere"}
            pageName={"matières"}
        />
    );
}

export default ManageSubjects;