import logoUlco from "../../assets/images/logoUlco.png"
import {Link} from "react-router-dom";

function Home() {
    return (
        <div className={"bg-[#dddddd] w-full h-full"}>
            <div className={"flex justify-center w-full"}>
                <img src={logoUlco} className={"w-[500px] pt-10"}/>
            </div>
            <div
                className="flex flex-col items-center space-y-6 w-full px-6 pt-28"
            >
                <div className={"text-center"}>
                    Ce site sert à gérer les promotions de l'ULCO, et ce qui y est associé (étudiants,
                    matières et notes).
                </div>
                <Link to={"/manage-promotions"} className={"underline"}>
                    Gérer les promotions
                </Link>
            </div>
        </div>
    )
}

export default Home;