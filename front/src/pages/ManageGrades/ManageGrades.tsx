import TableManagementPage from "../../components/TableManagement/TableManagementPage.tsx";
import {
    AdditionalColumnsDictType, MoreButtonLinksInfoArrayType, OriginalColumnsArrayType,
    ShowedColumnsLabelsDictType, ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

function ManageGrades() {
    const originalColumnsArray: OriginalColumnsArrayType = [
        "id",
        "note",
        "id_etudiant",
        "id_matiere"
    ] as const;

    const additionalColumnsDict: AdditionalColumnsDictType = {
        matiereIntitule: {
            apiUrl: "http://localhost:8080/matiere",
            id: "id_matiere",
            columnsToFetch: ["intitule"]
        },

        etudiantNomPrenom: {
            apiUrl: "http://localhost:8080/etudiant",
            id: "id_etudiant",
            columnsToFetch: ["nom", "prenom"]
        }
    } as const;

    const showedColumnsLabelsDict: ShowedColumnsLabelsDictType = {
        note: "Note",
        matiereIntitule: "Intitulé matière",
        etudiantNomPrenom: "Etudiant"
    } as const;

    const showedColumnsTypesDict: ShowedColumnsTypesDictType = {
        note: "float",
        matiereIntitule: "array",
        etudiantNomPrenom: "array"
    } as const;

    const moreButtonLinksInfoArray: MoreButtonLinksInfoArrayType = [{
        baseUrl: "/manage-students",
        columnsToGet: ["etudiantNomPrenom"],
        label: "Voir l'étudiant"
    }, {
        baseUrl: "/manage-subjects",
        columnsToGet: ["matiereIntitule"],
        label: "Voir la matière"
    }] as const;

    return (
        <TableManagementPage
            originalColumnsArray={originalColumnsArray}
            additionalColumnsDict={additionalColumnsDict}
            showedColumnsLabelsDict={showedColumnsLabelsDict}
            showedColumnsTypesDict={showedColumnsTypesDict}
            moreButtonLinksInfoArray={moreButtonLinksInfoArray}
            apiUrl={"http://localhost:8080/note"}
            pageName={"notes"}
        />
    );
}

export default ManageGrades;