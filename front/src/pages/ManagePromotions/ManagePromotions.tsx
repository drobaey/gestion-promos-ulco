import TableManagementPage from "../../components/TableManagement/TableManagementPage.tsx";
import {
    AdditionalColumnsDictType, OriginalColumnsArrayType, ShowedColumnsLabelsDictType,
    ShowedColumnsTypesDictType
} from "../../types/TableManagement/TableManagementTypes.ts";

function ManagePromotions() {
    const originalColumnsArray: OriginalColumnsArrayType = [
        "id",
        "intitule",
        "annee"
    ] as const;

    const additionalColumnsDict: AdditionalColumnsDictType = {} as const;

    const showedColumnsLabelsDict: ShowedColumnsLabelsDictType = {
        intitule: "Intitulé",
        annee: "Année"
    } as const;

    const showedColumnsTypesDict: ShowedColumnsTypesDictType = {
        intitule: "text",
        annee: "int"
    } as const;

    return (
        <TableManagementPage
            originalColumnsArray={originalColumnsArray}
            additionalColumnsDict={additionalColumnsDict}
            showedColumnsLabelsDict={showedColumnsLabelsDict}
            showedColumnsTypesDict={showedColumnsTypesDict}
            apiUrl={"http://localhost:8080/promotion"}
            pageName={"promotions"}
        />
    );
}

export default ManagePromotions;