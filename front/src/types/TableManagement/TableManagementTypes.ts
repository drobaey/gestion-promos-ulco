/**
 * Les colonnes présentes dans la table originale.
 */
export type OriginalColumnsArrayType = readonly string[];

/**
 * Les colonnes supplémentaires (à récupérer depuis une autre table). Chaque clé représente le nom d'une colonne
 * qu'on souhaite afficher dans le tableau (le nom est créé, ce n'est pas une colonne qui existe dans la BDD).
 * Ex : "promotionNameAndYear".
 */
export type AdditionalColumnsDictType = {
    [additionalColumnName: string]: {
        readonly apiUrl: string, // L'URL de l'API à appeler pour récupérer les données dans la table référencée
        readonly id: string, // Le nom de la clé étrangère référencée dans la table originelle
        readonly columnsToFetch: readonly string[] // Les colonnes à récupérer depuis la table référencée
    },
};

/**
 * Dictionnaire comportant les labels des colonnes qu'on souhaite afficher dans le tableau, avec en clé les noms
 * des colonnes (qui peuvent être des colonnes de la table originale ou des colonnes supplémentaires), et en valeur
 * le label qu'on souhaite afficher dans le tableau.
 * Ex : {note : "Note", matiereIntitule : "Intitulé matière", etudiantNomPrenom : "Etudiant"}
 */
export type ShowedColumnsLabelsDictType = {
    readonly [columnName: string]: string
};

/**
 * Dictionnaire comportant les types des colonnes qu'on souhaite afficher dans le tableau, avec en clé les noms des
 * colonnes (qui peuvent être des colonnes de la table originale ou des colonnes supplémentaires), et en valeur
 * le type de la colonne.
 * Ex : {note : "float", matiereIntitule : "array", etudiantNomPrenom : "array"}
 */
export type ShowedColumnsTypesDictType = {
    readonly [columnName: string]: "text" | "array" | "int" | "float" | "date"
};

/**
 * Tableau utilisé rediriger l'utilisateur vers une autre page via un dropdown / bouton "More" (les 3 points verticaux).
 * Si ce tableau est undefined, on n'affichera pas de dropdown / bouton "More".
 */
export type MoreButtonLinksInfoArrayType = ReadonlyArray<{
    readonly baseUrl: string, // l'url de base de la page vers laquelle on veut rediriger l'utilisateur
    readonly columnsToGet: readonly string[], // les colonnes à récupérer pour construire l'url
    readonly label: string // le label du lien
}>;

/**
 * Les valeurs des colonnes de la table originelle (et potentiellement les colonnes supplémentaires)
 */
export type RowType = {
    [columnName: string]: string | number;
};

/**
 * La clé est l'id de la ligne dans la table référencée, la valeur est la valeur de la colonne.
 */
export type PossibleValuesType = {
    [id_of_foreign_table: number]: string;
};

/**
 * Les valeurs possibles des colonnes supplémentaires, stockées sous forme de dictionnaire. Pour chaque colonne
 * supplémentaire, la clé est le nom de la colonne (ex : "promotionNameAndYear") et la valeur est un dictionnaire
 * contenant les valeurs possibles de la colonne supplémentaire. La clé est l'id de la ligne dans la table
 * référencée, la valeur est la valeur de la colonne.
 */
export type AdditionalColumnsPossibleValuesType = {
    // pour chaque colonne supplémentaire
    [additionalColumnName: string]: PossibleValuesType;
};
