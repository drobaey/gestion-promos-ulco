## Pour lancer le frontend :
Dans le dossier /front, exécutez les commandes suivantes :
```
npm install && npm run dev
```

Pour utiliser pleinement l'application, il est nécessaire de lancer le backend en même temps (voir le README.md du 
dossier /back).

## Technologies utilisées
ReactJS (via Vite), Tailwind CSS, Typescript

## Informations complémentaires
Nous n'avons pas précisé lors notre présentation que nous utilisions Typescript, car j'ai (Dennis ROBAEY) 
appris après cette présentation que j'allais devoir l'utiliser pour mon stage de fin d'année. J'ai donc voulu
me familiariser avec Typescript, du coup j'ai refait le projet avec Typescript.  

Le front a été designé pour être utilisé sur mobile principalement, d'où le style de l'application.

## Structure du projet

```
/front
├── /public
│   └──...           # Les fichiers publics (favicon, ...)
└── /src
    ├── /assets       # Les assets (images, ...)
    ├── /components   # Les composants réutilisables sur l'ensemble du projet / des pages différentes (inputs, etc)
    ├── /hooks        # Les hooks React personalisés
    ├── /types        # Les types Typescript personalisés
    └── /pages        # Les pages individuelles et les composants uniquement utilisés sur ces mêmes pages (organisées selon leur chemin dans l'URL, en CamelCase)
        ├── /Home     # Contient les composants de la page/route "/home", et les sous-dossiers des pages commençant par ce même chemin
        └── ...


Le fichier src/App.tsx gère notamment les différentes routes (via react-router-dom) de l'application.
Le fichier src/main.tsx est le point d'entrée de l'application.
Le fichier src/index.css contient la base du CSS utilisé.

Le fichier tailwind.config.ts contient la configuration utilisé pour Tailwind CSS.
```